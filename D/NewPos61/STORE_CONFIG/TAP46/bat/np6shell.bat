@echo off
SET NP6DRIVER=e:
SET NP6EXEDIR=e:\NewPOS61\bin
%NP6DRIVER%
cd %NP6EXEDIR%
if exist ..\bat\np6custom.bat goto :custom
if exist ..\bat\np6setenv.bat call ..\bat\np6setenv.bat
if "%NP6_POSDB%"  == "" SET NP6_POSDB=pos-db.xml
if "%NP6_DATDIR%" == "" SET NP6_DATDIR=..\POSDATA
if "%NP6_OUTDIR%" == "" SET NP6_OUTDIR=..\OUT
if "%NP6_TMPDIR%" == "" SET NP6_TMPDIR=..\TEMP
if "%NP6_PARAMS%" == "" SET NP6_PARAMS=pos-log61.properties
SET NP6_FLAGS=%1
if "%NP6_FLAGS%" == "" SET NP6_FLAGS=-I
SET NP6EXEC=npApp.exe "%NP6_DATDIR%\%NP6_POSDB%" "%NP6_OUTDIR%" "%NP6_TMPDIR%" %NP6_PARAMS%
if not exist ..\bat\start.np6 start updtmain.exe %NP6_FLAGS% NP6 "%NP6_DATDIR%" "%NP6_OUTDIR%" "%NP6_TMPDIR%" -A %NP6EXEC%
if exist ..\bat\start.np6 start updtmain.exe %NP6_FLAGS% NP6 "%NP6_DATDIR%" "%NP6_OUTDIR%" "%NP6_TMPDIR%" -F ..\bat\start.np6
goto :end
:custom
..\bat\np6custom.bat %1
:end
exit
