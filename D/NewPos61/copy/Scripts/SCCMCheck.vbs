'//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'// Name:   SCCM2012-Check
'// Func:   Check to see if SCCM is already installed.  If not, then install it
'//
'// Create: 	04/12/2010 - Rahul Sharma
'// Modify:
'//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'On Error Resume Next

Dim sSCCMinstall
'Dim strcmd

Const logfile = "D:\SCCMCheck.log"

Set fs = CreateObject("scripting.FileSystemObject")
set sh = CreateObject("wscript.shell")
Set objFSO = CreateObject("scripting.filesystemobject")
Set logStream = objFSO.createtextfile(logFile , True)

logStream.writeline Date & " : " & Time & ":: SCCM check V1.0 Started"

'read in the first argument
sSCCMinstall = Wscript.Arguments(0)  
logStream.writeline Date & " : " & Time & ":: SCCM Bat Name Passed to Script : " & sSCCMinstall
	
	if fs.FolderExists("D:\windows\ccmsetup") Then
    'Check to see if SCCM is already installed...
		If sccmcheck Then 
		logStream.writeline Date & " : " & Time & ":: SCCM previuosly Installed.  Installation not required"
		Wscript.quit
	    Else
		logStream.writeline Date & " : " & Time & ":: SCCM not Installed.  Installation Process Starting"
		strcmd = sSCCMinstall
		logStream.writeline Date & " : " & Time & ":: Command to Install SCCM = " & strcmd
		sh.Run strCmd,,True
		logStream.writeline Date & " : " & Time & ":: SCCM Installation Complete"
	    End If
	Else
		logStream.writeline Date & " : " & Time & ":: SCCM Installer NOT copied to local device..Aborting.."
        Wscript.quit		
	End If

logStream.Close

function sccmcheck()
dim bSCCMInstalled 
set service = GetObject ("winmgmts:")

for each Process in Service.InstancesOf ("Win32_Process")
	If Process.Name = "ccmexec.exe" or Process.Name = "ccmsetup.exe" then
		bSCCMInstalled=True
		Exit for
	Else
	bSCCMInstalled=False	
	End If
next
sccmcheck=bSCCMInstalled
End function
'///////////////////////////END MAIN////////////////////////////////////////

