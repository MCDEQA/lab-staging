Const JOIN_DOMAIN = 1
Const ACCT_CREATE = 2
Const ACCT_DELETE = 4
Const WIN9X_UPGRADE = 16
Const DOMAIN_JOIN_IF_JOINED = 32
Const JOIN_UNSECURE = 64
Const MACHINE_PASSWORD_PASSED = 128
Const DEFERRED_SPN_SET = 256
Const INSTALL_INVOCATION = 262144
Const NETSETUP_ACCT_DELETE = 2
 
 'Initialize WScript Shell
Set WshShell = WScript.CreateObject("WScript.Shell")
 
 strDomain = "FWORKGROUP"
strPassword = "Password"
strUser = "AD Account Username"
 
Set objNetwork = CreateObject("WScript.Network")
strComputer = objNetwork.ComputerName
 
Set objComputer = _
	GetObject("winmgmts:{impersonationLevel=Impersonate}!\\" & _
	strComputer & "\root\cimv2:Win32_ComputerSystem.Name='" & _
	strComputer & "'")
	
UnJoinReturnValue = objComputer.UnjoinDomainOrWorkGroup(NETSETUP_ACCT_DELETE)
 
ReturnValue = objComputer.JoinDomainOrWorkGroup(strDomain, _
	strPassword, _
	strDomain & "\" & strUser, _
	NULL)
	
WScript.Sleep 5000