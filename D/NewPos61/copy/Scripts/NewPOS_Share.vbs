'//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'// Name:   Share Newpos61  
'// Func:   Single script to share newpos61 folder in PR7 and XPe 
'//			On XPe Read write access to everyone and on PR7 Full control to Administrator
'// Create: 	12/11/14 - Rahul Sharma
'// Modify:
'//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
On Error Resume Next

Dim  sMajorVer
Dim  sCmdXPe
Dim  sCmdPR7
iKVS = -1  
iPOS = -1 
strComputer = "."

Set fs = CreateObject("scripting.FileSystemObject")
set sh = CreateObject("wscript.shell")

sMajorVer = sh.RegRead("HKLM\software\Xpestaging\Xpeimage\Major version number")

sCmdPR7_POS="D:\windows\system32\net.exe Share Newpos61=E:\Newpos61 /GRANT:NewPOS,FULL"
sCmdXPe_POS="D:\windows\system32\net.exe Share Newpos61=E:\Newpos61 /unlimited" 

sCmdPR7_KVS="D:\windows\system32\net.exe Share Newpos61=D:\Data\Newpos61 /GRANT:NewPOS,FULL"
sCmdXPe_KVS="D:\windows\system32\net.exe Share Newpos61=D:\Data\Newpos61 /unlimited" 

If fs.FolderExists("D:\Data\Xpestaging\") Then
	iKVS = 1
	iPOS = 0
else
	iKVS = 0
	iPOS = 1
End if

if (sMajorVer = 12 And iKVS) then
sh.Run sCmdPR7_KVS,,True
elseif (sMajorVer = 12 AND iPOS) then
sh.Run sCmdPR7_POS,,True
elseif (sMajorVer = 11 AND iKVS) then
sh.Run sCmdXPe_KVS,,True
else
sh.Run sCmdXPe_POS,,True
End if




