'//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'// Name:   Telequip T-flex config & test-check
'// Func:   Check to see if Telequip T-flex config is already installed.  If not, then install it
'//
'// Create: 	05/11/14 - Rahul Sharma
'// Modify:
'//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
On Error Resume Next

Dim  sTflexConfigInstall
Dim strcmd

Const logfile = "E:\temp\TflexConfigInstallCheck.log"

Set fs = CreateObject("scripting.FileSystemObject")
set sh = CreateObject("wscript.shell")
Set objFSO = CreateObject("scripting.filesystemobject")
Set logStream = objFSO.createtextfile(logFile , True)

logStream.writeline Date & " : " & Time & ":: TflexConfigInstall check V1.0 Started"
'read in the first argument
sTflexConfigInstall = Wscript.Arguments(0)  

logStream.writeline Date & " : " & Time & ":: T-flex Installer Name Passed to Script : " & sTflexConfigInstall

If fs.FileExists(sTflexConfigInstall) Then
    'Check to see if T-flex is already installed...
    If fs.FolderExists("d:\program files\Telequip\T-Flex Config & Test") Then 
		logStream.writeline Date & " : " & Time & ":: T-flex config Previously Installed.  Installation not required"
	Else
		logStream.writeline Date & " : " & Time & ":: T-flex config not Installed.  Installation Process Starting"
		strcmd = sTflexConfigInstall & " /sp- /silent /norestart""ADDLOCAL=ALL REBOOT=Suppress"""
		logStream.writeline Date & " : " & Time & ":: Command to Install T-flex config = " & strcmd
		sh.Run strCmd,,True
		logStream.writeline Date & " : " & Time & ":: T-flex config Install Completed"
	End If
Else
	logStream.writeline Date & " : " & Time & ":: T-flex config Install NOT copied to local device"
End If

logStream.Close
'///////////////////////////END MAIN////////////////////////////////////////

