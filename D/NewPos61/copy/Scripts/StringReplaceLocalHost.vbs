Function ReturnRegistryValue (RegPath)
Dim SysVarReg, RegValue
On Error Resume Next
Err.clear
Set SysVarReg = WScript.CreateObject("WScript.Shell")
RegValue = SysVarReg.RegRead(RegPath)
If Hex(Err.number) = "80070002" Then
' MsgBox "Not There"
ReturnRegistryValue = "NONE"
Else
' MsgBox RegValue
ReturnRegistryValue = RegValue
End If
SetSysVarReg = Nothing 
End Function

Const ForReading = 1
Const ForWriting = 2

strFileName = Wscript.Arguments(0)
strOldText = Wscript.Arguments(1)
strReplaceType = Wscript.Arguments(2)

dim strTemp1

if strReplaceType = "Host" then
  strNewText =  ReturnRegistryValue ("HKLM\Software\XPeStaging\Settings\Computer Name")
else
  strNewText =  ReturnRegistryValue ("HKLM\Software\XPeStaging\Settings\IP Address")
end if

' Dim objNet
' On Error Resume Next 

' Set objNet = CreateObject("WScript.NetWork")
' if strReplaceType = "Host" then
'  strNewText =  objNet.ComputerName
' else
'   For Each oIP in GetObject("winmgmts:").ExecQuery _
'  ("SELECT * FROM Win32_PingStatus WHERE address = '" & objNet.ComputerName & "'")
'   strNewText = oIP.ProtocolAddress
' Next
' end if
  

' Dim strInfo
' strInfo = "User Name is     " & objNet.UserName & vbCRLF & _
'          "Computer Name is " & objNet.ComputerName & vbCRLF & _
'          "Domain Name is   " & objNet.UserDomain
' MsgBox strInfo


' The following chunk of code is to remove any extra characters that are part of the server name
' For example - US00888HVS01.casper.mcditr.com will be replaced by US00888HVS01
' If just the US00888HVS01 is passed, it will be copied as is

' if strReplaceType = "Host" then
'   strTemp1 = InStr (strNewText, ".") - 1
'   if strTemp1 > 0 then
'      strNewText = Left (StrNewText, strTemp1)
'   end if
' end if

Set objFSO = CreateObject("Scripting.FileSystemObject")
IF objFSO.FileExists (strFileName) then
Set objFile = objFSO.OpenTextFile(strFileName, ForReading)

strText = objFile.ReadAll
objFile.Close
strNewText = Replace(strText, strOldText, strNewText)

Set objFile = objFSO.OpenTextFile(strFileName, ForWriting)
objFile.WriteLine strNewText
objFile.Close
END IF
Set objNet = Nothing   
