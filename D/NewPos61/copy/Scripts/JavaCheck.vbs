'//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'// Name:   JavaCheck
'// Func:   Check to see if Java is already installed.  If not, then install it
'//
'// Create: 	07/04/10 - R.Billingsby
'// Modify:
'//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
On Error Resume Next

Dim sJavaInstall
Dim strcmd

Const logfile = "JavaCheck.log"

Set fs = CreateObject("scripting.FileSystemObject")
set sh = CreateObject("wscript.shell")
Set objFSO = CreateObject("scripting.filesystemobject")
Set logStream = objFSO.createtextfile(logFile , True)

logStream.writeline Date & " : " & Time & ":: JavaCheck V1.0 Started"

'read in the first argument
sJavaInstall = Wscript.Arguments(0)  
logStream.writeline Date & " : " & Time & ":: Java Installer Name Passed to Script : " & sJavaInstall

If fs.FileExists(sJavaInstall) Then
    'Check to see if Java is already installed...
    If fs.FolderExists("d:\program files\Java") Then 
		logStream.writeline Date & " : " & Time & ":: Java Previously Installed.  Installation not required"
	Else
		logStream.writeline Date & " : " & Time & ":: Java not Installed.  Installation Process Starting"
		strcmd = sJavaInstall & " /s /v/qn""ADDLOCAL=ALL REBOOT=Suppress"""
		logStream.writeline Date & " : " & Time & ":: Command to Install Java = " & strcmd
		sh.Run strCmd,,True
		logStream.writeline Date & " : " & Time & ":: Java Install Completed"
	End If
Else
	logStream.writeline Date & " : " & Time & ":: Java Install NOT copied to local device"
End If

logStream.Close
'///////////////////////////END MAIN////////////////////////////////////////

