'//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'// Name:   NP6Check
'// Func:   Check to see if the NewPOS update service is started, if so stop it so that the Newpos 6 folder can be deleted
'//
'// Create: 	07/04/10 - R.Billingsby
'// Modify:
'//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
On Error Resume Next

Dim sNPFolder
Dim sUpdateName

Const logfile = "NP6Check.log"

Set fs = CreateObject("scripting.FileSystemObject")
set sh = CreateObject("wscript.shell")
Set objFSO = CreateObject("scripting.filesystemobject")
Set logStream = objFSO.createtextfile(logFile , True)

logStream.writeline Date & " : " & Time & ":: NP6Check V1.0 Started"

'read in the first argument
sNPFolder = Wscript.Arguments(0)  
logStream.writeline Date & " : " & Time & ":: NP6 Folder Name Passed to Script : " & sNPFolder

sUpdateName = sNPFolder & "bin\updtService.exe"
logStream.writeline Date & " : " & Time & ":: Update Services File Name : " & sUpdateName

If fs.FileExists(sUpdateName) Then
	logStream.writeline Date & " : " & Time & ":: Update Services File Found"
    'Find ut if the update service is running...
    If UpdateServiceRunning Then 
		'Stop the update service.
		strcmd = sUpdateName & " uninstall"
		logStream.writeline Date & " : " & Time & ":: Command to Uninstall the service = " & strcmd
		sh.Run strCmd,,True
	End If
End If

logStream.Close
'///////////////////////////END MAIN////////////////////////////////////////

Function UpdateServiceRunning

Dim sComputer
Dim bFound

	logStream.writeline Date & " : " & Time & ":: Starting Function to Check for Update Service Running"
	bFound = False
	sComputer = "."
	Set objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & sComputer & "\root\cimv2")
	Set colRunningServices = objWMIService.ExecQuery("Select * from Win32_Service")

	For Each objService in colRunningServices 
    	logStream.writeline Date & " : " & Time & ":: Service Found = " & objService.DisplayName & ", Service State = " & objService.State
		If UCase(objService.DisplayName) = "NEWPOS 6.1 UPDATE SERVICE" Then
	    	logStream.writeline Date & " : " & Time & "::****** Found NewPOS 6 Update Service"
	    	If objService.State = "Running" Then
		    	logStream.writeline Date & " : " & Time & "::****** NewPOS 6 Updates Service IS Running... This will be stopped"
		    	bFound = True
	    	End If
		End If
	Next
	UpdateServiceRunning = bFound
End Function

