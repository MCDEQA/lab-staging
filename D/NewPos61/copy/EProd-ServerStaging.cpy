;Version 1.0.1 - 18.02.2013

;Files to copy during the staging process for the E-Production Server and Bin applications
;following two steps are only needed when staging from a machine with pasisadapter already running. If PasisAdapter is running the delete later would fail. Causing the staging to fail.
MESSAGE=Stopping PasisAdapter service...; TIMEOUT=3 
RUN=d:\windows\system32\net.exe stop "PasisAdapter"; WAIT=YES
RUN=D:\data\eprod\pasisadapter.exe -unregServer; WAIT=YES
DELETE_FOLDER=D:\Data\eprod; OPTIONAL=YES

MESSAGE=Copying EPROD server binaries...; TIMEOUT=3 
SOURCE_FOLDER=<SERVER>\NewPos\COPY\eprod\SERVER_BIN\eprod; DESTINATION=D:\Data\
DELETE_FILE=D:\windows\pasis.ini; optional=yes
SOURCE_FILE=<SERVER>\NewPos\COPY\eprod\SERVER_BIN\windows\pasis.ini; DESTINATION=D:\windows\; OPTIONAL=YES
SOURCE_FILE=<SERVER>\Newpos\COPY\eprod\SERVER_BIN\applications.app; DESTINATION=D:\data\xpestaging\app\; OPTIONAL=YES; OVERWRITE=YES
SOURCE_FILE=<SERVER>\Newpos\COPY\eprod\SERVER_BIN\applications.app; DESTINATION=D:\xpestaging\app\; OPTIONAL=YES; OVERWRITE=YES

;Update UltraVNC Configuration for eggPlant Connectivity
SOURCE_FILE=<SERVER>\Newpos\copy\ultravnc.ini; DESTINATION="d:\program files\ultravnc\ultravnc.ini"; OVERWRITE=YES

RUN=D:\data\eprod\pasisadapter.exe -service