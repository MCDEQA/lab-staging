; Check for Apply Update Service
SOURCE_FILE=<SERVER>\NewPOS\copy\scripts\pos6check.vbs; DESTINATION=D:\Data\pos6check.vbs
SOURCE_FILE=<SERVER>\NewPOS\copy\scripts\NewPOS_Share.vbs; DESTINATION=D:\Data\NewPOS_Share.vbs; OVERWRITE=YES; OPTIONAL=YES
RUN=D:\windows\system32\wscript.exe d:\data\pos6check.vbs d:\data\newpos61\; WAIT=YES

;Stop NP6 Service
RUN=D:\Windows\System32\net.exe stop "NewPOS 6.1 Update Service"; WAIT=YES; OPTIONAL=YES
RUN=D:\Windows\System32\SC delete "NewPOS 6.1 Update Service" /Y; WAIT=YES
RUN=D:\windows\system32\net.exe Share Newpos61 /d; OPTIONAL=YES

; Uninstall current NP6 Version
DELETE_FOLDER=D:\Data\NewPos61; OPTIONAL=YES

; Copy new Folders for NP6 installation
SOURCE_FOLDER=<SERVER>\NewPOS\PosData; DESTINATION=D:\Data\NewPos61\; OVERWRITE=YES
SOURCE_FOLDER=<SERVER>\NewPOS\Bin; DESTINATION=D:\Data\NewPos61\; OVERWRITE=YES
SOURCE_FOLDER=<SERVER>\NewPOS\Bat; DESTINATION=D:\Data\NewPos61\; OVERWRITE=YES
SOURCE_FOLDER=<SERVER>\NewPOS\wayweb; DESTINATION=D:\Data\NewPos61\; OVERWRITE=YES

; Copy SENP6Update
SOURCE_FOLDER=<SERVER>\NewPOS\copy\SENP6\update; DESTINATION=D:\; OVERWRITE=YES 

; delete waystation file
RUN=d:\windows\system32\cmd.exe /c del D:\Data\NewPOS61\bat\START_STORECONTROLLER.NP6; WAIT=YES; OPTIONAL=YES

;Update UltraVNC Configuration for eggPlant Connectivity
SOURCE_FILE=<SERVER>\Newpos\copy\ultravnc.ini; DESTINATION=D:\program files\ultravnc\ultravnc.ini; OVERWRITE=YES

; Install and start Apply Update
RUN=D:\Data\newpos61\bin\updtservice.exe install; WAIT=YES
RUN=d:\windows\system32\net.exe start "NewPOS 6.1 Update Service"

;Copy Configuration files
SOURCE_FOLDER=<SERVER>\NewPOS\Store_Config\ORB<KVS00>\bat; DESTINATION=D:\DATA\NewPos61\; OVERWRITE=YES; OPTIONAL=YES
SOURCE_FILE=<SERVER>\NewPOS\Store_Config\app\ORB\*.*; DESTINATION=D:\XPeStaging\APP\; OVERWRITE=YES; OPTIONAL=YES
SOURCE_FILE=<SERVER>\NewPOS\Store_Config\app\ORB\*.*; DESTINATION=D:\data\XPeStaging\APP\; OVERWRITE=YES; OPTIONAL=YES

;Share newpos directory
;RUN=D:\windows\system32\net.exe Share Newpos61=d:\data\Newpos61 /grant:administrator,full /remark:Posv6; WAIT=YES; OPTIONAL=YES

;Run script to share irrespective of XPe,PR7,KVS & POS.
RUN=D:\windows\system32\wscript.exe D:\data\NewPOS_Share.vbs; WAIT=YES 
