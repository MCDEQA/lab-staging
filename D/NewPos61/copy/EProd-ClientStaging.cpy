;Version 1.0.0 - 18.02.2013

;Files to copy during the staging process for the E-Production Server and Bin applications
;following two steps are only needed when staging from a machine with pasisadapter running to one without pasisadapter. If PasisAdapter is running the delete later would fail. Causing the staging to fail.
MESSAGE=Stopping PasisAdapter service...; TIMEOUT=3 
RUN=d:\windows\system32\net.exe stop "PasisAdapter"; WAIT=YES
RUN=D:\data\eprod\pasisadapter.exe -unregServer; WAIT=YES
DELETE_FOLDER=D:\Data\eprod; OPTIONAL=YES

MESSAGE=Copying EPROD client binaries...; TIMEOUT=3 
SOURCE_FOLDER=<SERVER>\NewPos\copy\eprod\CLIENTS\eprod; DESTINATION=D:\Data\ OVERWRITE=YES
SOURCE_FILE=<SERVER>\Newpos\copy\eprod\CLIENTS\applications.app; DESTINATION=d:\data\xpestaging\app\; OVERWRITE=YES; OPTIONAL=YES
SOURCE_FILE=<SERVER>\Newpos\copy\eprod\CLIENTS\applications.app; DESTINATION=d:\xpestaging\app\; OVERWRITE=YES; OPTIONAL=YES

;Update UltraVNC Configuration for eggPlant Connectivity
SOURCE_FILE=<SERVER>\Newpos\copy\ultravnc.ini; DESTINATION=D:\program files\ultravnc\ultravnc.ini; OVERWRITE=YES

;Below line is not generic should be update by market as per the requirement, its now set for KVS03 and KVS07
SOURCE_FILE=<SERVER>\NewPOS\copy\eprod\CLIENTS\eprod\KVS\KVS<POS00>_KvsInfConfig.xml; DESTINATION=d:\Data\eprod\KVS\KvsInfConfig.xml
