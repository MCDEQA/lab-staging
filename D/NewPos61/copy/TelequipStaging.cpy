;Files to copy each time the POS stages

MESSAGE="Copying Telequip Driver files";TIMEOUT=3
SOURCE_FOLDER=<SERVER>\newpos\Copy\Telequip; DESTINATION=E:\Temp\; OVERWRITE=YES
SOURCE_FILE=<SERVER>\newpos\Copy\scripts\T-FlexConfigCheck.vbs; DESTINATION=E:\Temp\; OVERWRITE=YES

;Install the Telequip main utility
MESSAGE="Installing Telequip Test & Configure Software";TIMEOUT=3
RUN=D:\windows\system32\wscript.exe E:\Temp\T-FlexConfigCheck.vbs E:\Temp\Telequip\T-Flex_Config_Test_V4_70_Setup.exe; WAIT=YES

;Install the VComm driver
MESSAGE="Installing Telequip Virtual Comm Driver Software";TIMEOUT=3
RUN=E:\Temp\Telequip\TelequipVComm109Setup.exe /sp- /verysilent /norestart;WAIT=YES
RUN="C:\Program Files\telequip VComm\vsbsetup.exe";WAIT=YES
RUN=regsvr32.exe vsport.dll /i:"Crane Payment Solutions#0000UW-E8J770-PU3U83-YVK1BH-VAK22Y-1BTX4E-BCF36D-2600CE-38FA52-207B47-2C2CEA-806793" /s;WAIT=YES
RUN="C:\Program Files\telequip VComm\tqcom.exe" /COM /7;WAIT=YES
RUN="C:\Program Files\telequip VComm\winserv.exe" install TelequipVComm -displayname TelequipVcomm -description TelequipVComm109 -start auto -errorcontrol ignore -noninteractive "VComm.exe";WAIT=YES
RUN="C:\Program Files\telequip VComm\winserv.exe" start TelequipVComm;WAIT=YES
