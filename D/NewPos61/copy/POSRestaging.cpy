;Stop UPDT Service
RUN=D:\Windows\System32\net.exe stop "NewPOS 6.1 Update Service"; WAIT=YES; OPTIONAL=YES
RUN=D:\Windows\System32\SC delete "NewPOS 6.1 Update Service" /Y; WAIT=YES
RUN=D:\windows\system32\net.exe Share Newpos61 /d; OPTIONAL=YES

;Tlog Backup
SOURCE_FOLDER=E:\NewPOS61\out; DESTINATION=E:\NP6_BACKUP\; OVERWRITE=YES; OPTIONAL=YES
SOURCE_FOLDER=E:\NewPOS61\outputfiles; DESTINATION=E:\NP6_BACKUP\; OVERWRITE=YES; OPTIONAL=YES
DELETE_FOLDER=D:\update
DELETE_FILE=D:\Xpestaging\App\*.*; WAIT=YES
DELETE_FOLDER=E:\NewPos61

;Copy Hosts, Config and Market.ini
SOURCE_FILE=<SERVER>\XPeImage\Tools\Staging\*.*; DESTINATION=D:\xpestaging\; OVERWRITE=YES
SOURCE_FILE=<SERVER>\XPeImage\Tools\localize\*.*; DESTINATION=D:\xpestaging\; OVERWRITE=YES

; Remove Device From Domain
SOURCE_FILE=<SERVER>\NewPOS\copy\scripts\RemoveDomain.vbs; DESTINATION=D:\xpestaging\RemoveDomain.vbs; OVERWRITE=YES; OPTIONAL=YES
RUN=D:\windows\system32\wscript.exe  D:\xpestaging\RemoveDomain.vbs; WAIT=YES

