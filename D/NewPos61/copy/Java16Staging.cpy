;Version 1.0.1 - 18.02.2013
;Process to run for installing Java including the necessary eProduction extensions
MESSAGE = Copying Java v16 for Eprod....; TIMEOUT=3
DELETE_FOLDER=D:\Data\Java
SOURCE_FOLDER=<SERVER>\NewPos\COPY\Java16\SETUP; DESTINATION=D:\Data\Java\
;uninstall
RUN=D:\windows\system32\msiexec.exe /x d:\data\java\setup\jre1.6.0_29.msi /qn /norestart; OPTIONAL=YES WAIT=YES
;install
MESSAGE = Installing Java v16 for Eprod....; TIMEOUT=3
RUN=D:\windows\system32\msiexec.exe /i d:\data\java\setup\jre1.6.0_29.msi TARGETDIR="D:\Program Files\Java" ADDLOCAL=ALL SYSTRAY=0 JAVAUPDATE=0 AUTOUPDATECHECK=0 JU=0 EULA=1 /qn /norestart /l*v D:\Data\JavaInst.log; WAIT=YES
SOURCE_FILE=<SERVER>\NewPos\COPY\Java16\extensions\comm.jar; DESTINATION=D:\Program Files\Java\jre6\lib\ext\; OVERWRITE=YES
SOURCE_FILE=<SERVER>\NewPos\COPY\Java16\extensions\win32com.dll; DESTINATION=D:\Program Files\Java\jre6\bin\; OVERWRITE=YES
SOURCE_FILE=<SERVER>\NewPos\COPY\Java16\extensions\javax.comm.properties; DESTINATION=D:\Program Files\Java\jre6\lib\; OVERWRITE=YES
DELETE_FOLDER=D:\Data\Java
