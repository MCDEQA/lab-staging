;Copying sotec drivers
;DELETE_FOLDER=D:\Temp\SOTEC_BB
MESSAGE = Copying Bumpbar binaries...; TIMEOUT=2
SOURCE_FOLDER=<SERVER>\NewPOS\Copy\SOTEC-BB\system32; DESTINATION=D:\windows\; OVERWRITE=YES
SOURCE_FOLDER=<SERVER>\NewPOS\Copy\SOTEC-BB\inf; DESTINATION=D:\windows\; OVERWRITE=YES
SOURCE_FOLDER=<SERVER>\NewPOS\Copy\SOTEC-BB\drivers; DESTINATION=D:\windows\system32\; OVERWRITE=YES

MESSAGE=Installing sotec bumpbar drivers; TIMEOUT=3
RUN=D:\windows\system32\USB_BB.exe /SILENT /NORESTART /LOG=E:\SOTEC_BB.log; WAIT=YES
SOURCE_FILE=<SERVER>\NewPOS\Copy\SOTEC-BB\Disable-BumpBar-Enumeration.reg; DESTINATION=D:\;
APP=regedit.exe /s D:\Disable-BumpBar-Enumeration.reg