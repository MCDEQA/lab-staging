;Back up the PosFiles folder to the BO server
MESSAGE=Copying PosFiles folder to the backoffice; TIMEOUT=40
DELETE_FOLDER=<SERVER>\NewPos\STORE_CONFIG\BACKUP\CSO<POS00>
SOURCE_FOLDER=C:\NewPos61\OUT; DESTINATION=<SERVER>\NewPos\STORE_CONFIG\BACKUP\POS<POS00>\
SOURCE_FOLDER=C:\NewPos61\OutPutFiles; DESTINATION=<SERVER>\NewPos\STORE_CONFIG\BACKUP\POS<POS00>\
DELETE_FOLDER=D:\update

;Copy Hosts, Config and Market.ini
SOURCE_FILE=<SERVER>\XPeImage\Tools\Staging\*.*; DESTINATION=D:\xpestaging\; OVERWRITE=YES
SOURCE_FILE=<SERVER>\XPeImage\Tools\localize\*.*; DESTINATION=D:\xpestaging\; OVERWRITE=YES

; Remove Device From Domain
SOURCE_FILE=<SERVER>\NewPOS\copy\scripts\RemoveDomain.vbs; DESTINATION=D:\xpestaging\RemoveDomain.vbs; OVERWRITE=YES; OPTIONAL=YES
RUN=D:\windows\system32\wscript.exe  D:\xpestaging\RemoveDomain.vbs; WAIT=YES
