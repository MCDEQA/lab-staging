;Files to copy during the staging process for the POS device
MESSAGE=Checking for existing NP6 installation; TIMEOUT=3
SOURCE_FILE=<SERVER>\NewPOS\copy\scripts\pos6check.vbs; DESTINATION=E:\Newpos61\pos6check.vbs
RUN=c:\windows\system32\wscript.exe "E:\Newpos61\pos6check.vbs" "E:\Newpos61\"; WAIT=YES

;Stop NP6 Service
RUN=D:\Windows\System32\net.exe stop "NewPOS 6.1 Update Service"; WAIT=YES; OPTIONAL=YES
RUN=D:\Windows\System32\SC delete "NewPOS 6.1 Update Service" /Y; WAIT=YES
RUN=D:\windows\system32\net.exe Share Newpos61 /d; OPTIONAL=YES

; Uninstall current NP6 Version
DELETE_FOLDER=E:\NewPos61; OPTIONAL=YES

SOURCE_FOLDER=<SERVER>\NewPos\Bin; DESTINATION=E:\NewPos61\ ; OVERWRITE=YES
SOURCE_FOLDER=<SERVER>\NewPos\PosData; DESTINATION=E:\Newpos61\ ; OVERWRITE=YES
SOURCE_FOLDER=<SERVER>\NewPos\KioskData; DESTINATION=E:\Newpos61\ ; OVERWRITE=YES; OPTIONAL=YES
SOURCE_FOLDER=<SERVER>\NewPos\Ngk; DESTINATION=E:\Newpos61\ ; OVERWRITE=YES
SOURCE_FOLDER=<SERVER>\NewPos\bat; DESTINATION=E:\Newpos61\ ; OVERWRITE=YES

; delete waystation file
RUN=d:\windows\system32\cmd.exe /c del E:\NewPOS61\bat\START_STORECONTROLLER.NP6; WAIT=YES; OPTIONAL=YES

; Copy SENP6Update
SOURCE_FOLDER=<SERVER>\NewPOS\copy\SENP6\update; DESTINATION=D:\; OVERWRITE=YES

; Copy GQP-Tools
SOURCE_FOLDER=<SERVER>\NewPOS\copy\GQP-Tools; DESTINATION=D:\; OVERWRITE=YES


;Configuration files
SOURCE_FILE=<SERVER>\NewPos\STORE_CONFIG\POS<POS00>\bat\start.np6; DESTINATION=E:\Newpos61\bat\start.np6; OVERWRITE=YES
SOURCE_FILE=<SERVER>\Newpos\STORE_CONFIG\POS<POS00>\bat\np6shell.bat; DESTINATION=E:\Newpos61\bat\np6shell.bat; OVERWRITE=YES
SOURCE_FILE=<SERVER>\NewPOS\Store_Config\app\NGK\*.*; DESTINATION=D:\XPeStaging\App; OVERWRITE=YES; OPTIONAL=YES

;Update UltraVNC Configuration for eggPlant Connectivity
SOURCE_FILE=<SERVER>\Newpos\copy\ultravnc.ini; DESTINATION=D:\program files\ultravnc\ultravnc.ini; OVERWRITE=YES

;Start NP6 Services
RUN=E:\Newpos61\bin\updtservice.exe install;WAIT=YES
RUN=E:\Newpos61\bin\updtservice.exe start;WAIT=YES