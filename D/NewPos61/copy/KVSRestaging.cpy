RUN=D:\Windows\System32\net.exe stop "NewPOS 6.1 Update Service"; WAIT=YES; OPTIONAL=YES
RUN=D:\Windows\System32\SC delete "NewPOS 6.1 Update Service" /Y; WAIT=YES
RUN=D:\windows\system32\net.exe Share Newpos61 /d; OPTIONAL=YES
DELETE_FOLDER=D:\Data\NewPos61
DELETE_FOLDER=D:\update
DELETE_FILE=D:\data\Xpestaging\App\*.*; WAIT=YES; OPTIONAL=YES
DELETE_FILE=D:\Xpestaging\App\*.*; WAIT=YES; OPTIONAL=YES

;Copy Hosts, Config and Market.ini
SOURCE_FILE=<SERVER>\XPeImage\Tools\Staging\*.*; DESTINATION=D:\xpestaging\; OVERWRITE=YES; OPTIONAL=YES
SOURCE_FILE=<SERVER>\XPeImage\Tools\localize\*.*; DESTINATION=D:\xpestaging\; OVERWRITE=YES; OPTIONAL=YES
SOURCE_FILE=<SERVER>\XPeImage\Tools\Staging\*.*; DESTINATION=D:\data\xpestaging\; OVERWRITE=YES; OPTIONAL=YES
SOURCE_FILE=<SERVER>\XPeImage\Tools\localize\*.*; DESTINATION=D:\data\xpestaging\; OVERWRITE=YES; OPTIONAL=YES

; Remove Device From Domain
SOURCE_FILE=<SERVER>\NewPOS\copy\scripts\RemoveDomain.vbs; DESTINATION=D:\xpestaging\RemoveDomain.vbs; OVERWRITE=YES; OPTIONAL=YES
RUN=D:\windows\system32\wscript.exe  D:\xpestaging\RemoveDomain.vbs; WAIT=YES
SOURCE_FILE=<SERVER>\NewPOS\copy\scripts\RemoveDomain.vbs; DESTINATION=D:\data\xpestaging\RemoveDomain.vbs; OVERWRITE=YES; OPTIONAL=YES
RUN=D:\windows\system32\wscript.exe  D:\data\xpestaging\RemoveDomain.vbs; WAIT=YES