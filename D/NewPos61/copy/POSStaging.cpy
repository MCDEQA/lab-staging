;Files to copy during the staging process for the POS device
SOURCE_FILE=<SERVER>\NewPOS\copy\scripts\pos6check.vbs; DESTINATION=E:\pos6check.vbs
SOURCE_FILE=<SERVER>\NewPOS\copy\scripts\NewPOS_Share.vbs; DESTINATION=E:\NewPOS_Share.vbs; OVERWRITE=YES; OPTIONAL=YES
RUN=D:\windows\system32\wscript.exe E:\pos6check.vbs E:\newpos61\; WAIT=YES

;Stop NP6 Service
RUN=D:\Windows\System32\net.exe stop "NewPOS 6.1 Update Service"; WAIT=YES; OPTIONAL=YES
RUN=D:\Windows\System32\SC delete "NewPOS 6.1 Update Service" /Y; WAIT=YES
RUN=D:\windows\system32\net.exe Share Newpos61 /d; OPTIONAL=YES

; Uninstall current NP6 Version
DELETE_FOLDER=E:\NewPos61; OPTIONAL=YES

SOURCE_FOLDER=<SERVER>\NewPOS\PosData; DESTINATION=E:\NewPos61\; OVERWRITE=YES 
SOURCE_FOLDER=<SERVER>\NewPOS\bat; DESTINATION=E:\NewPos61\; OVERWRITE=YES 
SOURCE_FOLDER=<SERVER>\NewPOS\wayweb; DESTINATION=E:\NewPos61\; OVERWRITE=YES 
SOURCE_FOLDER=<SERVER>\NewPOS\Bin; DESTINATION=E:\NewPos61\; OVERWRITE=YES

; Copy SENP6Update
SOURCE_FOLDER=<SERVER>\NewPOS\copy\SENP6\update; DESTINATION=D:\; OVERWRITE=YES 

;Update UltraVNC Configuration for eggPlant Connectivity
SOURCE_FILE=<SERVER>\Newpos\copy\ultravnc.ini; DESTINATION=D:\program files\ultravnc\ultravnc.ini; OVERWRITE=YES

; delete waystation file
RUN=d:\windows\system32\cmd.exe /c del E:\NewPOS61\bat\START_STORECONTROLLER.NP6; WAIT=YES; OPTIONAL=YES

RUN=E:\newpos61\bin\updtservice.exe install; WAIT=YES
RUN=D:\windows\system32\net.exe start "NewPOS 6.1 Update Service"

;Configuration files
SOURCE_FOLDER=<SERVER>\NewPOS\Store_Config\POS<POS00>\bat; DESTINATION=E:\NewPos61\; OVERWRITE=YES; OPTIONAL=YES
SOURCE_FILE=<SERVER>\NewPOS\Store_Config\app\POS\*.*; DESTINATION=D:\XPeStaging\App; OVERWRITE=YES; OPTIONAL=YES

;Run script to share irrespective of XPe,PR7,KVS & POS.
RUN=D:\windows\system32\wscript.exe E:\NewPOS_Share.vbs; WAIT=YES 