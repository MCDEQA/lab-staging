;TAPNP6 11/06/14
;Stop UPDT Service
RUN=D:\Windows\System32\net.exe stop "NewPOS 6.1 Update Service"; WAIT=YES; OPTIONAL=YES
RUN=D:\Windows\System32\SC delete "NewPOS 6.1 Update Service" /Y; WAIT=YES
RUN=D:\windows\system32\net.exe Share Newpos61 /d; OPTIONAL=YES

MESSAGE = Saving out folder to backoffice for backup; TIMEOUT=4
DELETE_FOLDER=<SERVER>\Newpos\Out\Tlog\Backup\<POS00>
SOURCE_FILE=E:\NewPos61\Out\npevt_POS00<POS00>.tpa; DESTINATION=<SERVER>\Newpos\Out\Tlog\Backup\<POS00>\Out\; OVERWRITE=YES; OPTIONAL=YES
SOURCE_FILE=E:\NewPos61\Out\nptrx_POS00<POS00>.tpa; DESTINATION=<SERVER>\Newpos\Out\Tlog\Backup\<POS00>\Out\; OVERWRITE=YES; OPTIONAL=YES
SOURCE_FILE=E:\NewPos61\Out\np61Debug-0.*; DESTINATION=<SERVER>\Newpos\Out\Tlog\Backup\<POS00>\Out\; OVERWRITE=YES; OPTIONAL=YES
SOURCE_FILE=E:\NewPos61\Out\np61Errors-0.*; DESTINATION=<SERVER>\Newpos\Out\Tlog\Backup\<POS00>\Out; OVERWRITE=YES; OPTIONAL=YES
SOURCE_FILE=E:\NewPos61\Out\np61Fatal-0.*; DESTINATION=<SERVER>\Newpos\Out\Tlog\Backup\<POS00>\Out\; OVERWRITE=YES; OPTIONAL=YES
SOURCE_FILE=E:\NewPos61\Out\np61Warn-0.*; DESTINATION=<SERVER>\Newpos\Out\Tlog\Backup\<POS00>\Out\; OVERWRITE=YES; OPTIONAL=YES
SOURCE_FOLDER=E:\NewPos61\Out\Reprint61; DESTINATION=<SERVER>\Newpos\Out\Tlog\Backup\<POS00>\Out\; OVERWRITE=YES; OPTIONAL=YES

;Copy Hosts, Config and Market.ini
SOURCE_FILE=<SERVER>\XPeImage\Tools\Staging\*.*; DESTINATION=D:\xpestaging\; OVERWRITE=YES
SOURCE_FILE=<SERVER>\XPeImage\Tools\localize\*.*; DESTINATION=D:\xpestaging\; OVERWRITE=YES

; Remove Device From Domain
SOURCE_FILE=<SERVER>\NewPOS\copy\scripts\RemoveDomain.vbs; DESTINATION=D:\xpestaging\RemoveDomain.vbs; OVERWRITE=YES; OPTIONAL=YES
RUN=D:\windows\system32\wscript.exe  D:\xpestaging\RemoveDomain.vbs; WAIT=YES



