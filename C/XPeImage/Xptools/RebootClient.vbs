' ****************************************************************************
' Title: ClientReboot.vbs
'
' Description:    This script will reboot this computer.
'
' $Revision:   1.0  $
'
' $Log:   //PUCK/SOURCE_MGMT/ARCHIVES/Magellan/archives/Runtime/client/UNIVERSAL/xptools/RebootClient.vbs-arc  $
'
'   Rev 1.0   11 Jun 2003 16:43:40   NealS
'Initial revision.
'
'   Rev 1.0   23 May 2003 10:08:50   LovelaceM
'Initial revision.
'
' ****************************************************************************
'
option explicit

Dim objService, objItem

Set objService = GetObject("winmgmts:{impersonationlevel=impersonate,(shutdown)}").Execquery("Select * From win32_operatingsystem")
For Each objItem in objService
    objItem.Reboot()
Next

