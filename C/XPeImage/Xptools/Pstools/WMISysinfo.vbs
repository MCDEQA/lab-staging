'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Title = WMI System Information
' Description = Provides Hardware and System Information on NewPOS POS
' Author = Richard Harrison, Andrew Cheeseman
'
' Modification History:
'---
' Karim.Poonja@us.mcd.com  2005-10-10  Added Processor & BIOS info
' Dexter.Fountain@us.mcd.com  2006-04-03 Added AppCheck script
'Karim.Poonja@us.mcd.com  2006-06-07  Updated to refelct Global OS changes
' Dexter.Fountain@us.mcd.com 2007-08-09 Creates temp folder if needed
' McCoyK@us.panasonic.com 2007-10-12 Updated to report System Uptime
'---
' Keywords = VBScript, WSH, WMI
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Require Variables to be explicitly declared 
Option Explicit


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Set Constants
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Const HKEY_CURRENT_USER = &H80000001
Const HKEY_LOCAL_MACHINE = &H80000002
Const HKLM = &H80000002

Const ForReading = 1, ForWriting = 2, ForAppending = 8
Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0
Const TemporaryFolder = 2  
Const EVT_TYPE_ERROR = 1
Const EVT_TYPE_WARNING = 2
Const EVT_TYPE_INFO = 4

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' ***** Variable Initialization *****
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Dim fso, ts, tfolder, i, MB, KB, Gb, System, os, Service, NIC, Processor, SerialPort, BIOS
Dim sServerName,sUserName,sDomainRole, sDomainRoleName
Dim objOSset, objServiceset, objSystemSet, objNICSet, objPartitionSet, objDiskSet
Dim Partition, objShell, Disk, objProcessorSet, objSerialPortSet, objBIOSSet
Dim objWMIService, objProcessor
Dim colOperatingSystems, objOS, dtmBootup, dtmLastBootupTime, dtmSystemUptime, n

Dim objCODSet,codInfo
Dim strComputer

strComputer = "."

i  = 0
Gb = 1073741824
Mb = 1048576

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Object Initialization
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Set fso = CreateObject("Scripting.FileSystemObject")
Set objShell = Wscript.CreateObject("Wscript.Shell")

Dim sVarNewPOSPath
sVarNewPOSPath = objShell.RegRead("HKLM\SOFTWARE\GUM\POSv5 Folder")

Const FILE_SYSINFO = "E:\temp\sysinfo.txt"
Dim FILE_CHANGELOG
FILE_CHANGELOG = sVarNewPOSPath & "\bin\changelog.txt"

'WMI Object to gather information from

Set objOSset = GetObject("winmgmts:").InstancesOf("Win32_OperatingSystem")
Set objServiceset = GetObject("winmgmts:").InstancesOf("Win32_Service")
Set objSystemSet = GetObject("winmgmts:").InstancesOf ("Win32_ComputerSystem")
Set objNICSet = GetObject("winmgmts:").InstancesOf ("Win32_NetworkAdapterConfiguration")
Set objPartitionSet = GetObject("winmgmts:").InstancesOf ("Win32_LogicalDisk")
Set objDiskSet = GetObject("winmgmts:").InstancesOf ("Win32_DiskDrive")
Set objProcessorSet = GetObject("winmgmts:").InstancesOf ("Win32_Processor")
Set objSerialPortSet = GetObject("winmgmts:").InstancesOf ("Win32_SerialPort")
Set objBIOSSet = GetObject("winmgmts:").InstancesOf ("Win32_BIOS")
Set objCODSet = GetObject("winmgmts:").InstancesOf ("MCD_CustomerOrderDisplay")



'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' ***** MAIN Body of Script *****
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
On Error Resume Next
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Gather initial information about the current users and Machine Role
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
For Each System in objSystemSet
	sUserName = System.UserName
	sServerName = System.Name
	sDomainRole = System.DomainRole
Next

Select Case  sDomainRole 
	Case 0
		sDomainRoleName = "Standalone Workstation"
	Case 1
		sDomainRoleName = "Member Workstation"
	Case 2
		sDomainRoleName = "Standalone Server"
	Case 3
		sDomainRoleName = "Member Server"
	Case 4
		sDomainRoleName = "Backup Domain Controller"
	Case 5
		sDomainRoleName = "Primary Domain Controller"	
	Case Else
		sDomainRoleName = "Discription Unknown Role Number ("&sDomainRole&")"
End Select

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Sets up the TEXT file to be written to
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Set tfolder = fso.GetSpecialFolder(TemporaryFolder)
if not fso.FolderExists("e:\temp") then fso.CreateFolder("e:\temp") 

if fso.FileExists( FILE_SYSINFO ) then fso.DeleteFile( FILE_SYSINFO)

Set ts = fso.OpenTextFile(FILE_SYSINFO, ForWriting, TristateUseDefault)

'Write the report head data
ts.WriteLine "Report Run By = " & SuserName
ts.WriteLine "Report File = " & FILE_SYSINFO
ts.WriteLine "Date and Time = " & Now

ts.WriteLine " "
ts.WriteLine "[OS]"

'Write information from the Win32_OperatingSystem object
for each os in objOSset 
      ts.WriteLine "Operating System = " & os.Caption
      ts.WriteLine "Operating System Version = " & os.Version
      ts.WriteLine "Operating System SPK = " & os.CSDVersion
      ts.WriteLine "Operating System Serial No = " & os.SerialNumber
      ts.WriteLine "System Drive = " & os.SystemDevice
      ts.WriteLine "System Directory = " & os.SystemDirectory
      ts.WriteLine "Windows Directory = " & os.WindowsDirectory
next

ts.WriteLine " "
ts.WriteLine "[SERVICES]"

'Information from the Win32_Service object
for each Service in objServiceset 
	If Service.State = "Running" then
      		ts.WriteLine Service.Caption & ", " & service.StartMode
	End If      		
next

ts.WriteLine " "
ts.WriteLine "[SYSTEM]"

'Write information from the Win32_objComputerSystem object
for each System in objSystemSet
	ts.WriteLine "System Name = " & System.Name
	ts.WriteLine "System Domain = " & System.Domain
	ts.WriteLine "System Domain Role = " & sDomainRoleName
	ts.WriteLine "System Manufacturer= " & System.Manufacturer
	ts.WriteLine "System Model = " & System.Model
	ts.WriteLine "System Processors = " & System.NumberOfProcessors
	ts.WriteLine "System Type = " & System.SystemType
	ts.WriteLine "Total Physical Memory = " & FormatNumber(System.TotalPhysicalMemory/Mb,2) & " Mb"
next

'Write information from the Win32_DiskDrive object
for each Disk in objDiskSet
	ts.WriteLine "Disk Size = " & FormatNumber(Disk.Size/Gb,2) & " Gb"
	if Not IsNull(Disk.Availability) then ts.WriteLine "Available Disk Space = " & FormatNumber(Disk.Availability/Gb,2) & " Gb"
	ts.WriteLine "Disk Model = "& Disk.Model  
	ts.WriteLine "Install Date = "& Disk.InstallDate  
	ts.WriteLine "ErrorDesc = "& Disk.ErrorDescription  
	ts.WriteLine "Last Error Code = "& Disk.LastErrorCode  
	ts.WriteLine "Disk Partitions = "& Disk.Partitions  
	ts.WriteLine "S.M.A.R.T Disk Status = "& Disk.Status   
next

'Write information for SystemUpTime
Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
Set colOperatingSystems = objWMIService.ExecQuery("Select * from Win32_OperatingSystem")
 
For Each objOS in colOperatingSystems
    dtmBootup = objOS.LastBootUpTime
    dtmLastBootupTime = WMIDateStringToDate(dtmBootup)
    dtmSystemUptime = DateDiff("n", dtmLastBootUpTime, Now)
    ts.Writeline "System Uptime = " & dtmSystemUptime & " minutes"
Next

ts.WriteLine " "
ts.WriteLine "[Network Adapter Configuration]"

'Write information from the Win32_NetworkAdapterConfiguration object
For each NIC in objNICSet
	If NIC.IPEnabled = "True" or NIC.IPXEnabled = "True" Then
		ts.WriteLine "{Adapter (" & NIC.Index & ")" & Chr(9) & "Device Name ("& NIC.ServiceName & ")}"
		ts.WriteLine "Discription = " & NIC.Description 
		ts.WriteLine "MAC Address = " & NIC.MACAddress
		ts.WriteLine "IP Enabled = " & NIC.IPEnabled
		ts.WriteLine "IPX Enabled = " & NIC.IPXEnabled
		ts.WriteLine " "
	End If
	If NIC.IPEnabled = "True" Then
		ts.WriteLine " "
		ts.WriteLine ":TCP/IP Configuration for Adapter(" &NIC.Index& ")"
		ts.WriteLine "DHCP Enabled = " & NIC.DHCPServer
		ts.WriteLine "Netbios TCP Options = " & NIC.TcpipNetbiosOptions
		i=0
		For i = LBound(NIC.IPaddress) to UBound(NIC.IPaddress)
		    ts.writeLine "IP Address " & i
		    ts.writeLine Chr(9) & "IP Address = " & NIC.IPAddress(i) 
		    ts.writeLine Chr(9) & "IP Sub Net = " & NIC.IPSubnet(i)		
			ts.writeLine " "
		Next
		
		if Not IsNull(NIC.DefaultIPGateway) then
			i=0
			For i = LBound(NIC.DefaultIPGateway) to UBound(NIC.DefaultIPGateway)
				ts.writeLine "Default Gateway " & i & " = " & NIC.DefaultIPGateway (i) 
			Next
		end if
		
		if Not IsNull(NIC.DNSServerSearchOrder) then
			i=0
			For i = LBound(NIC.DNSServerSearchOrder) to UBound(NIC.DNSServerSearchOrder)
				ts.writeLine "DNS Setting " & i & " = " & NIC.DNSServerSearchOrder(i) 
			Next
		end if
			
	   	If NIC.WINSEnableLMHostsLookup = "True"  Then
	   		ts.WriteLine "Primary WINS Server = " & NIC.WINSPrimaryServer
	   		ts.WriteLine "Secondary WINS Server = " & NIC.WINSSecondaryServer
		Else
	   		ts.WriteLine "No Wins Service Configured"
	   	End if
	End If
	   	
	If NIC.IPXEnabled = "True" Then
		ts.WriteLine " "
		ts.WriteLine ":IPX Configuration for Adapter(" & NIC.Index & ")"
		ts.writeLine "IPX Virtual Net Number = " & NIC.IPXVirtualNetNumber	
		i=0
		For i = LBound(NIC.IPXFrameType) to UBound(NIC.IPXFrameType)
			Select Case NIC.IPXFrameType(i)
		  		Case "0"
	    			ts.writeLine Chr(9) & "IPX FrameType = Ethernet II"
	    			ts.writeLine Chr(9) & "IPX Address = " & NIC.IPXNetworkNumber(i)
	    		Case "1"
	    			ts.writeLine Chr(9) & "IPX FrameType = Ethernet 802.3"
	    			ts.writeLine Chr(9) & "IPX Address = " & NIC.IPXNetworkNumber(i)
	    		Case "2"
	    			ts.writeLine Chr(9) & "IPX FrameType = Ethernet 802.2"
	    			ts.writeLine Chr(9) & "IPX Address = " & NIC.IPXNetworkNumber(i)
	    		Case "3"
	    			ts.writeLine Chr(9) & "IPX FrameType = Ethernet SNAP"
	    			ts.writeLine Chr(9) & "IPX Address = " & NIC.IPXNetworkNumber(i)
	    		Case "255"
	    			ts.writeLine Chr(9) & "IPX FrameType = Auto Discover"
	    		Case Else
	    			ts.writeLine Chr(9) & "IPX FrameType = Un-Known Frame Type"
	    	End Select
		Next
		ts.writeLine " "
	End If	
Next

'#####################
ts.WriteLine " "
ts.WriteLine "[Identifying Processor Type]"

'Determines the processor architecture (such as x86 or ia64) for a specified computer. 

Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
Set objProcessor = objWMIService.Get("win32_Processor='CPU0'")
 
If objProcessor.Architecture = 0 Then
    ts.WriteLine "This is an x86 computer."
ElseIf objProcessor.Architecture = 1 Then
    ts.WriteLine "This is a MIPS computer."
ElseIf objProcessor.Architecture = 2 Then
    ts.WriteLine "This is an Alpha computer."
ElseIf objProcessor.Architecture = 3 Then
    ts.WriteLine "This is a PowerPC computer."
ElseIf objProcessor.Architecture = 6 Then
    ts.WriteLine "This is an ia64 computer."
Else
    ts.WriteLine "The computer type could not be determined."
End If

ts.WriteLine " "
ts.WriteLine "[Processor Information]"
'Returns information about the processors installed on a computer. 

For Each Processor in objProcessorSet
'    ts.WriteLine "Address Width: " & Processor.AddressWidth
'    ts.WriteLine "Architecture: " & Processor.Architecture
'    ts.WriteLine "Availability: " & Processor.Availability
    ts.WriteLine "CPU Status: " & Processor.CpuStatus
    ts.WriteLine "Current Clock Speed: " & Processor.CurrentClockSpeed
'    ts.WriteLine "Data Width: " & Processor.DataWidth
    ts.WriteLine "Description: " & Processor.Description
'    ts.WriteLine "Device ID: " & Processor.DeviceID
'    ts.WriteLine "Ext Clock: " & Processor.ExtClock
'    ts.WriteLine "Family: " & Processor.Family
'    ts.WriteLine "L2 Cache Size: " & Processor.L2CacheSize
'    ts.WriteLine "L2 Cache Speed: " & Processor.L2CacheSpeed
'    ts.WriteLine "Level: " & Processor.Level
    ts.WriteLine "Load Percentage: " & Processor.LoadPercentage
    ts.WriteLine "Manufacturer: " & Processor.Manufacturer
    ts.WriteLine "Maximum Clock Speed: " & Processor.MaxClockSpeed
    ts.WriteLine "Name: " & Processor.Name
'    ts.WriteLine "PNP Device ID: " & Processor.PNPDeviceID
'    ts.WriteLine "Processor Id: " & Processor.ProcessorId
'    ts.WriteLine "Processor Type: " & Processor.ProcessorType
'    ts.WriteLine "Revision: " & Processor.Revision
'    ts.WriteLine "Role: " & Processor.Role
'    ts.WriteLine "Socket Designation: " & Processor.SocketDesignation
'    ts.WriteLine "Status Information: " & Processor.StatusInfo
'    ts.WriteLine "Stepping: " & Processor.Stepping
'    ts.WriteLine "Unique Id: " & Processor.UniqueId
'    ts.WriteLine "Upgrade Method: " & Processor.UpgradeMethod
'    ts.WriteLine "Version: " & Processor.Version
'    ts.WriteLine "Voltage Caps: " & Processor.VoltageCaps
Next

ts.WriteLine " "
ts.WriteLine "[Serial Port Properties]"
'Returns information about the serial ports installed on a computer. 

For Each SerialPort in objSerialPortSet
    
    ts.WriteLine "Description: " & SerialPort.Description
    ts.WriteLine "Binary: " & SerialPort.Binary
    ts.WriteLine "Device ID: " & SerialPort.DeviceID
    ts.WriteLine "Maximum Baud Rate: " & SerialPort.MaxBaudRate
'    ts.WriteLine "Maximum Input Buffer Size: " & SerialPort.MaximumInputBufferSize
'    ts.WriteLine "Maximum Output Buffer Size: " & SerialPort.MaximumOutputBufferSize
    ts.WriteLine "Name: " & SerialPort.Name
    ts.WriteLine "OS Auto Discovered: " & SerialPort.OSAutoDiscovered
    ts.WriteLine "PNP Device ID: " & SerialPort.PNPDeviceID
    ts.WriteLine "Provider Type: " & SerialPort.ProviderType
'    ts.WriteLine "Settable Baud Rate: " & SerialPort.SettableBaudRate
'    ts.WriteLine "Settable Data Bits: " & SerialPort.SettableDataBits
'    ts.WriteLine "Settable Flow Control: " & SerialPort.SettableFlowControl
'    ts.WriteLine "Settable Parity: " & SerialPort.SettableParity
'    ts.WriteLine "Settable Parity Check: " & SerialPort.SettableParityCheck
'    ts.WriteLine "Settable RLSD: " & SerialPort.SettableRLSD
'    ts.WriteLine "Settable Stop Bits: " & SerialPort.SettableStopBits
'    ts.WriteLine "Supports 16-Bit Mode: " & SerialPort.Supports16BitMode
    ts.WriteLine "Supports DTRDSR: " & SerialPort.SupportsDTRDSR
    ts.WriteLine "Supports Elapsed Timeouts: " & SerialPort.SupportsElapsedTimeouts
    ts.WriteLine "Supports Int Timeouts: " & SerialPort.SupportsIntTimeouts
    ts.WriteLine "Supports Parity Check: " & SerialPort.SupportsParityCheck
    ts.WriteLine "Supports RLSD: " & SerialPort.SupportsRLSD
    ts.WriteLine "Supports RTSCTS: " & SerialPort.SupportsRTSCTS
    ts.WriteLine "Supports Special Characters: " & SerialPort.SupportsSpecialCharacters
    ts.WriteLine "Supports XOn XOff: " & SerialPort.SupportsXOnXOff
    ts.WriteLine "Supports XOn XOff Setting: " & SerialPort.SupportsXOnXOffSet
    ts.WriteLine " "
Next
set objSerialPortSet = Nothing

ts.WriteLine " "
ts.WriteLine "[Retrieving BIOS Information]"
'Retrieves BIOS information for a computer, including BIOS version number and release date.

strComputer = "."
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
For each BIOS in objBIOSSet
    ts.WriteLine "Build Number: " & BIOS.BuildNumber
    ts.WriteLine "Current Language: " & BIOS.CurrentLanguage
    ts.WriteLine "Installable Languages: " & BIOS.InstallableLanguages
    ts.WriteLine "Manufacturer: " & BIOS.Manufacturer
    ts.WriteLine "Name: " & BIOS.Name
    ts.WriteLine "Primary BIOS: " & BIOS.PrimaryBIOS
    ts.WriteLine "Release Date: " & BIOS.ReleaseDate
    ts.WriteLine "Serial Number: " & BIOS.SerialNumber
    ts.WriteLine "SMBIOS Version: " & BIOS.SMBIOSBIOSVersion
    ts.WriteLine "SMBIOS Major Version: " & BIOS.SMBIOSMajorVersion
    ts.WriteLine "SMBIOS Minor Version: " & BIOS.SMBIOSMinorVersion
    ts.WriteLine "SMBIOS Present: " & BIOS.SMBIOSPresent
    ts.WriteLine "Status: " & BIOS.Status
    ts.WriteLine "Version: " & BIOS.Version
    For i = 0 to Ubound(BIOS.BiosCharacteristics)
        ts.WriteLine "BIOS Characteristics: " & _
            BIOS.BiosCharacteristics(i)
    Next
Next

'#########################
ts.WriteLine " "
ts.WriteLine "[Partitions]"

'Write information from the Win32_LogicalDisk object
for each Partition in objPartitionSet
		
	If Partition.Description = "Local Fixed Disk" then
		ts.WriteLine "{" & Partition.Name &" Volume}"
		ts.WriteLine Chr(9) & "Lable = " & Partition.VolumeName
		ts.WriteLine Chr(9) & "Serial No = " & Partition.VolumeSerialNumber
		if Not IsNull( Partition.Size ) then ts.WriteLine Chr(9) & "Size = " & FormatNumber(Partition.Size/Gb,2) & " Gb" 
		if Not IsNull( Partition.FreeSpace ) then ts.WriteLine Chr(9) & "FreeSpace = " & FormatNumber(Partition.FreeSpace/Gb,2) & " Gb" 
		ts.WriteLine Chr(9) & "File System = " & Partition.FileSystem
		ts.WriteLine Chr(9) & "Partition Size = " & Partition.Size
		ts.WriteLine Chr(9) & "Partition FreeSpace = " & Partition.FreeSpace
		ts.WriteLine " "
	End If
next

ts.WriteLine " "
ts.WriteLine "[Active Directory Information]"
ts.WriteLine " "
If sDomainRole < 5 Then
	ts.WriteLine "This is not a Domain Controller"
Else
	aFSMOConfig = GetFSMOConfig(sServerName)
	ts.WriteLine aFSMOConfig(0)
	ts.WriteLine aFSMOConfig(1)
	ts.WriteLine aFSMOConfig(2)
	ts.WriteLine aFSMOConfig(3)
	ts.WriteLine aFSMOConfig(4)
End if

'''
' AppCheck logic
'''

Dim strKeyPath, strValueName, strValue
Dim arrSubKeys, strSubkey
Dim oReg

ts.WriteLine " "
ts.WriteLine "[DUA Patch Information]"

strKeyPath = "SYSTEM\CurrentControlSet\Services\DUAgent\Parameters\Config\Sessions\0000"
strValueName = "cmdfile"

Set oReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" &_
 strComputer & "\root\default:StdRegProv")
oReg.GetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
Set oReg = Nothing

ts.WriteLine "Next DUA Release: " & strValue

ts.WriteLine " "
ts.WriteLine "[Installed Applications]"

Dim strKey, strEntry1a, strEntry1b, strEntry2, strEntry3, strEntry4, strEntry5
Dim intRet1, strValue1, strValue2, intValue3, intValue4, intValue5

strKey = "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\"
strEntry1a = "DisplayName"
strEntry1b = "QuietDisplayName"
strEntry2 = "InstallDate"
strEntry3 = "VersionMajor"
strEntry4 = "VersionMinor"
strEntry5 = "EstimatedSize"

Set oReg = GetObject("winmgmts://" & strComputer & _
 "/root/default:StdRegProv")
oReg.EnumKey HKLM, strKey, arrSubkeys

ts.WriteLine "The Following Applications Are Installed:" 
For Each strSubkey In arrSubkeys

  intRet1 = oReg.GetStringValue(HKLM, strKey & strSubkey, _
   strEntry1a, strValue1)
  If intRet1 <> 0 Then
    oReg.GetStringValue HKLM, strKey & strSubkey, _
     strEntry1b, strValue1
  End If
  If strValue1 <> "" Then
    ts.WriteLine VbCrLf & "Display Name: " & strValue1
  End If
  oReg.GetStringValue HKLM, strKey & strSubkey, _
   strEntry2, strValue2
  If strValue2 <> "" Then
    ts.WriteLine "Install Date: " & strValue2
  End If
  oReg.GetDWORDValue HKLM, strKey & strSubkey, _
   strEntry3, intValue3
  oReg.GetDWORDValue HKLM, strKey & strSubkey, _
   strEntry4, intValue4
  If intValue3 <> "" Then
     ts.WriteLine "Version: " & intValue3 & "." & intValue4
  End If
  oReg.GetDWORDValue HKLM, strKey & strSubkey, _
   strEntry5, intValue5
  If intValue5 <> "" Then
    ts.WriteLine "Estimated Size: " & Round(intValue5/1024, 3) & " megabytes"
  End If
Next
Set oReg = Nothing


'Obtains XPe OS major and minor release version information from the registry
ts.WriteLine " "
ts.WriteLine "[Data from XPeStaging Application Registry Files]"

Dim sVariableMaj, sVariableMin, sVariableHW, sVariablePOSNo, sVariableStoreNo, sVariableMkt, sVariableTZ, sVariableHFix

sVariableMaj = objShell.RegRead("HKLM\SOFTWARE\XPeStaging\XPeImage\Major Version Number")
sVariableMin = objShell.RegRead("HKLM\SOFTWARE\XPeStaging\XPeImage\Minor Version Number")
sVariableHFix = objShell.RegRead("HKLM\SOFTWARE\XPeStaging\XPeImage\Hotfix Version Number")
sVariableHW = objShell.RegRead("HKLM\SOFTWARE\XPeStaging\XPeImage\Hardware Model")
sVariablePOSNo = objShell.RegRead("HKLM\SOFTWARE\XPeStaging\Settings\POS Number")
sVariableStoreNo = objShell.RegRead("HKLM\SOFTWARE\XPeStaging\Settings\Store Number")
sVariableMkt = objShell.RegRead("HKLM\SOFTWARE\XPeStaging\Settings\Market")
sVariableTZ = objShell.RegRead("HKLM\SOFTWARE\XPeStaging\Settings\Time Zone")

ts.WriteLine "XPe OS Release: " & svariableMaj & "." & sVariableMin & "." & sVariableHFix
ts.WriteLine "Hardware Model: " & sVariableHW
ts.WriteLine "POS #: " & sVariablePOSNo
ts.WriteLine "Store #: " & sVariableStoreNo
ts.WriteLine "Market: " & sVariableMkt
ts.WriteLine "Time Zone: " & sVariableTZ


'Obtains information about GUM Packages

ts.WriteLine " "
ts.WriteLine "[GUM Packages]"
 
strKeyPath = "SOFTWARE\GUM\Packages\Installed"
Set oReg = GetObject("winmgmts://" & strComputer & _
 "/root/default:StdRegProv")
oReg.EnumKey HKLM, strKeyPath, arrSubKeys
Set oReg = Nothing

For Each strSubkey In arrSubKeys
    ts.WriteLine "Installed GUM Package:" & strSubkey
Next

ts.WriteLine " "
strKeyPath = "SOFTWARE\GUM\Packages\Failed"
Set oReg = GetObject("winmgmts://" & strComputer & _
 "/root/default:StdRegProv")
oReg.EnumKey HKLM, strKeyPath, arrSubKeys
set oReg = Nothing


For Each strSubkey In arrSubKeys
    ts.WriteLine "Failed GUM Package:" & strSubkey
Next

ts.WriteLine " "
 
strKeyPath = "SOFTWARE\GUM\Packages\Not Applicable"
Set oReg = GetObject("winmgmts://" & strComputer & _
 "/root/default:StdRegProv")
oReg.EnumKey HKLM, strKeyPath, arrSubKeys
Set oReg = Nothing

For Each strSubkey In arrSubKeys
    ts.WriteLine "Not Applicable GUM Package:" & strSubkey
Next



Dim objTextFile, LineNum, strNextLine, arrLineList

ts.WriteLine " "
If fso.FileExists(FILE_CHANGELOG) Then
	ts.WriteLine "[NewPOS Version Information from " & FILE_CHANGELOG & " ]"
	Set objTextFile = fso.OpenTextFile(FILE_CHANGELOG, ForReading)
	LineNum = 0
	While not objTextFile.AtEndOfStream
    		strNextLine = objTextFile.Readline
		LineNum = LineNum + 1
    		arrLineList = Split(strNextLine , ",")
    		ts.WriteLine arrLineList(0)
    		For i = 1 to Ubound(arrLineList)
        	ts.WriteLine arrLineList(i)
    	Next
	Wend
Else
	ts.WriteLine "NewPOS Version Information not available"
End If

objTextFile.close
set objTextFile = Nothing

ts.WriteLine " "
ts.WriteLine "[COD Inventory - via serial script]"
'Output the COD Inventory
For each codInfo in objCODSet
	ts.writeline "COD Manufacturer: " & codInfo.Manufacturer
	ts.writeline "COD Serial#: " & codInfo.SerialNumber
	ts.writeline "COD COM PORT: " & codInfo.comport
	ts.writeline "COD POS TYPE: " & codInfo.CODPOSType			
	ts.writeline "COD Temperature: " & codInfo.InternalTemperature
	ts.writeline "COD LastPOSData: " & codInfo.LastRcvPOSData
	ts.writeline "COD Firmware: " & codInfo.Firmware
	ts.writeline "COD Total RAM: " & codInfo.TotalFlashMemory
	ts.writeline "COD Free RAM: " & codInfo.FreeFlashMemory
	ts.writeline "COD System Board Info: " & codInfo.SystemBoardInfo
	ts.writeline "COD Screen Size: " & codInfo.ScreenSize 
	ts.writeline "COD Resolution: " & codInfo.Resolution
	ts.writeline "COD OCS Date-Time: " & codInfo.OCSDateTime
	ts.writeline "COD App Date-Time: " & codInfo.AppDateTime
Next

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Write event to event log
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
ts.Close
set ts = Nothing

objShell.LogEvent EVT_TYPE_INFO, "WMI Report Run By = " & SuserName
objShell.LogEvent EVT_TYPE_INFO, "WMI Report File = " & tfolder & "\" & sServerName & ".txt"

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Free allocated objects
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Set objShell = nothing
Set objProcessor = Nothing
Set objWMIService = Nothing
Set objBIOSSet = Nothing
Set objSerialPortSet = Nothing
Set objProcessorSet = Nothing
Set objDiskSet = Nothing
Set objPartitionSet = Nothing
Set objNICSet = Nothing
Set objSystemSet = Nothing
Set objServiceset = Nothing
Set objOSset = Nothing
Set tfolder = Nothing
Set fso = Nothing


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' ***** END OF SCRIPT *****
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

WScript.Quit

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' ***** Functions *****
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'********************************************************************
'*
'* Function     : GetFSMOConfig
'* Purpose      : To Gather FSMO Information from Domain Controller
'* Input        : Domain Contoller Name = Path
'* Input        : ObjectID
'*
'********************************************************************

Function  GetFSMOConfig(path)
Dim objNetwork, objArgs, ADOconnObj, bstrADOQueryString, objDSE, objRS
Dim objFMSO,objCompNTDS, objComputer
Dim aArray(4)

Set objNetwork = CreateObject("WScript.Network")

Set ADOconnObj = CreateObject("ADODB.Connection")

ADOconnObj.Provider = "ADSDSOObject"
ADOconnObj.Open "ADs Provider"


'PDC FSMO
bstrADOQueryString = "<LDAP://"&Path&">;(&(objectClass=domainDNS)(fSMORoleOwner=*));adspath;subtree"
Set objDSE = GetObject("LDAP://RootDSE")
Set objRS = ADOconnObj.Execute(bstrADOQueryString)
Set objFMSO = GetObject(objRS.Fields(0).Value)
Set objCompNTDS = GetObject("LDAP://" & objFMSO.fSMORoleOwner)
Set objComputer = GetObject(objCompNTDS.Parent)
aArray(0) = "The PDC FSMO is: " & objComputer.dnsHostName

'Rid FSMO
bstrADOQueryString = "<LDAP://"&Path&">;(&(objectClass=rIDManager)(fSMORoleOwner=*));adspath;subtree"

Set objRS = ADOconnObj.Execute(bstrADOQueryString)
Set objFMSO = GetObject(objRS.Fields(0).Value)
Set objCompNTDS = GetObject("LDAP://" & objFMSO.fSMORoleOwner)
Set objComputer = GetObject(objCompNTDS.Parent)
aArray(1) = "The RID FSMO is: " & objComputer.dnsHostName

'Infrastructure FSMO
bstrADOQueryString = "<LDAP://"&Path&">;(&(objectClass=infrastructureUpdate)(fSMORoleOwner=*));adspath;subtree"

Set objRS = ADOconnObj.Execute(bstrADOQueryString)
Set objFMSO = GetObject(objRS.Fields(0).Value)
Set objCompNTDS = GetObject("LDAP://" & objFMSO.fSMORoleOwner)
Set objComputer = GetObject(objCompNTDS.Parent)
aArray(2) = "The Infrastructure FSMO is: " & objComputer.dnsHostName

'Schema FSMO
bstrADOQueryString = "<LDAP://"&objDSE.Get("schemaNamingContext")&_
                     ">;(&(objectClass=dMD)(fSMORoleOwner=*));adspath;subtree"

Set objRS = ADOconnObj.Execute(bstrADOQueryString)
Set objFMSO = GetObject(objRS.Fields(0).Value)
Set objCompNTDS = GetObject("LDAP://" & objFMSO.fSMORoleOwner)
Set objComputer = GetObject(objCompNTDS.Parent)
aArray(3) = "The Schema FSMO is: " & objComputer.dnsHostName

'Domain Naming FSMO
bstrADOQueryString = "<LDAP://"&objDSE.Get("configurationNamingContext")&_
                     ">;(&(objectClass=crossRefContainer)(fSMORoleOwner=*));adspath;subtree"

Set objRS = ADOconnObj.Execute(bstrADOQueryString)
Set objFMSO = GetObject(objRS.Fields(0).Value)
Set objCompNTDS = GetObject("LDAP://" & objFMSO.fSMORoleOwner)
Set objComputer = GetObject(objCompNTDS.Parent)
aArray(4) = "The Domain Naming FSMO is: " & objComputer.dnsHostName
 
GetFSMOConfig = aArray 

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Free allocated objects
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Set objComputer = Nothing
Set objCompNTDS = Nothing
Set objFMSO = Nothing
Set objRS = Nothing
Set objDSE = Nothing
Set ADOconnObj = Nothing
Set objNetwork = Nothing

End Function

' 
'********************************************************************
'*
'* Function     : WMIDateStringToDate
'* Purpose      : converts a UTC datetime value to regular date-time value
'* Input        : UTC datetime
'* Output       : Standard DateTime (minutes)
'*
'********************************************************************
Function WMIDateStringToDate(dtmBootup)
    WMIDateStringToDate = CDate(Mid(dtmBootup, 5, 2) & "/" & _
        Mid(dtmBootup, 7, 2) & "/" & Left(dtmBootup, 4) _
            & " " & Mid (dtmBootup, 9, 2) & ":" & _
                Mid(dtmBootup, 11, 2) & ":" & Mid(dtmBootup,13, 2))
End Function