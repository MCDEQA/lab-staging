'CODrsm.vbs Script Version 2.5 Updated on 02/27/07 by SJJ
'CODRSM.vbs Written on 01/04/2007 by Scott/ J. Johnson and David Schuster
'This script is designed for the detection of multi COD
'platforms and executing RSM asset collection for log generation.
'=============================================
'--------------HEADER SECTION-----------------
'=============================================
'Option Explicit
'On Error resume next
'Declare all variables
Dim objCOD   	'holder for COD device
Dim vPOStype  	'holder for config listed in POSdb.xml
Dim vCODtype  	'holder for config set on COD firmware.
Dim strGetCODcom 'holder for complete ascii COD send command
Dim strRcvCODcom 'holder for ascii COD Response
Dim com    		'holder for com control with mscomm32.ocx
Dim strBuffer  	'holder for return serial data from COD
Dim objFSO   	'container for text file operation
Dim objFolder 	'holder for drive folder location
Dim objCodFile  'holder for COD update filename
Dim vDidUpdate	'holder for yes/no on update action
Dim objCUexe	'holder for COD update exe file
Dim objCUmod1	'holder for comand line modifyers
Dim objCUmod2	'holder for comand line modifyers
Dim strCUS		'holder for the string to execute the CU command
Dim iPort		'integer for COD serial port number
Dim cksum 		'variable for EB cksum calculation
Dim sh			'Shell for Wscript
Dim fs
Dim colInventory
Dim colCmdConsts
Dim colDescs
Dim isMFG
Dim strMsg

'Create shell for read/write of file
set sh = CreateObject("wscript.shell")
set fs = CreateObject("Scripting.FileSystemObject")

'//Load the COD XML config file
set xmlDoc = CreateObject("Msxml2.DOMDocument")
xmlDoc.async = false
xmlDoc.load("cod.xml")
xmlDoc.setProperty "SelectionLanguage", "XPath"

'//Create the collection object 
set colInventory = CreateObject("scripting.dictionary")


'//Create a collection of description variables from the XML
'//Creates colDescs Collection/Dictionary
CreateDescCol

'//Determine MFG
'//Interogate the com port or read an XML file
strMfg = GetMfg("//cods/cod")
 
'//Collect the inventory data based upon the commands set in the MFG specific inventory nodes
'//of the XML 
GetInventory strMfg,"//cods/cod[@mfg='" & strMfg &"']/inventory/cmds",false

'//Close the COMM PORT
com.PortOpen = False

'//Create the MIF
bMifEnabled = cbool(xmlDoc.SelectSingleNode("//mif").GetAttribute("enabled"))
if bMifEnabled then 
	ScreenEcho "MIF Enabled...Creating MIF File"
	CreateMIF
else
	ScreenEcho "MIF DISABLED"
End if

'//Write the data to the WMI MCD_CustomerOrderDisplay class
'//Will Create the class if it does not exist
bWMIEnabled = cbool(xmlDoc.SelectSingleNode("//wmiclass").GetAttribute("enabled"))
if bWMIEnabled then 
	ScreenEcho "WMI Updates Enabled...Updating WMI"
	UpdateWMI
else
	screenEcho "WMI Updates Disabled"

end if


'///////////////////////////////
'//Send the update
'// - Update functionality to be added
'///////////////////////////////


'/////////////////////////////End Main//////////////////////////////


'///////////////////////////////////////////////
'//Name:	InitializeCom
'//			Opens and sets the COM port 
'//			to values specified in the XML file
'//Returns: True if port opens successfully
'//			False if port cannot be opened.
'///////////////////////////////////////////////

Function InitializeCom (intComPort)

	InitializeCom = False
	Set com = CreateObject("MSCommLib.MSComm") 'set environment to send serial commands
	'//Read and set comm setting from XML
	com.CommPort = cint(intComPort)
	com.Settings = xmlDoc.selectSingleNode("//commsettings/settings").text
	com.RThreshold = cint(xmlDoc.selectSingleNode("//commsettings/rthreshold").text)
	com.InputLen = cint(xmlDoc.selectSingleNode("//commsettings/inputlen").text)
	com.InputMode = cint(xmlDoc.selectSingleNode("//commsettings/inputmode").text)
	com.InBufferSize = cint(xmlDoc.selectSingleNode("//commsettings/inbuff").text)
	com.OutBufferSize = cint(xmlDoc.selectSingleNode("//commsettings/outbuff").text)

	'//Open the port
	com.PortOpen = True

	if err then 
		strMsg = "Error opening COM Port: " & err.number
		ScreenEcho strMsg
		sh.LogEvent 1,strMsg
	else
		ScreenEcho "Success opening COM port"
		InitializeCom = True
	End if
	
End Function




Sub SendUpdate

'================================================
' Look for COD update.zip (ocs.zip)
'================================================
'Check for file

objCodFile = xmlDoc.selectSingleNode("//updatesettings/codupdatefile").text
objCUexe = xmlDoc.selectSingleNode("//updatesettings/updateExe").text
objCUmod1 = xmlDoc.selectSingleNode("//updatesettings/ExeMod01").text
objCUmod2 = xmlDoc.selectSingleNode("//updatesettings/ExeMod02").text
iPort = cint(xmlDoc.selectSingleNode("//commsettings/port").text)

SH.CurrentDirectory = "D:\rsm\cod"
If objFSO.FileExists(objCodFile) Then
'================================================
' Send update to COD
'================================================
 	strCUS = objCUexe & " " & objCUmod1 & iPort & " " & objCUmod2 & objCODFile 
 	oCurDir = SH.CurrentDirectory
	ScreenEcho "Currnet Directory: " & oCurDir
	ScreenEcho "OCS Command Line: " & strCUS
	'SH.CurrentDirectory = 
	set oExec = sh.exec(strCUS)

	strOutput = oExec.StdOut.ReadAll
	intExitCode = oExec.ExitCode
	
	ScreenEcho "Output: " & strOutput
	ScreenEcho "ExitCode: " & intExitCode

	
 	vDidUpdate = 1
 Else
 	vDidUpdate = 0
 	
 End If
 
'=================================================
'Send Gen 2 COD commands to device via comm
'then collect data and bind to strings, hold for output to mif
'=================================================
'If statement to wait if update was performed. This will wait 20sec to let COD reboot.
If vDidUpdate = 1 then
	for i = 1 to 25
		ScreenEcho i & " second(s)"
		WScript.Sleep 1000
	next 
Else
	Wscript.Sleep 500 '15000
End If
End Sub 

'/////////////////////////////////////////////////////
'//Name:	GETMFG
'//Func:	Returns the name of the COD MFG
'//Inputs: 	xmlPath - The path in the XML that allows
'//		the script to enumerate all of the MFG's
'//		initialization and inventory commands
'///////////////////////////////////////////////////

Function GetMfg(xmlPath)

Dim oINITCODs,oINITCOD
Dim strINIT
Dim colComPorts,oComPort
Dim objWMIService
Dim intComPort


isMFG = False
GetMFG = null


'//Get the list of comm ports installed on this device...
On Error Resume Next
strComputer = "."
Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
Set colComPorts = objWMIService.ExecQuery("Select * from Win32_SerialPort",,48)
For Each oComPort in colComPorts
	'//Parse the com port number from the DEVICEID
    	intComPort = Mid(oComPort.DeviceID,4)

	screenEcho "Testing " & oComPort.DeviceID & "..."


	if InitializeCOM(intComPort) then
		ScreenEcho oComPort.DeviceID & " port successfully initialized"
	else
		ScreenEcho "Error intitializing " & oComPort.DeviceID
	end if


	ScreenEcho "Testing for COD on " & oComPort.DeviceID

	'//Get collection of COD mfg from XML
	set oINITCODs = xmlDoc.selectNodes(xmlPath)

	'//Enumerate the COD MFGs
	For each oINITCOD in oINITCODs
		'//Get the initialization string
	
		strMFG = oINITCOD.GetAttribute("mfg")
		ScreenEcho "Testing for MFG: " & strMFG & " on " & oComPort.DeviceID
	
		'//Create the collection of MFG commands for the current MFG
		CreateMFGCol(strMFG)
	
		'//Call the get inventory command...Sends the initialization commands
		'//to the COD from the XML...if the COD responds with output then 
		'//then the current MFG is set to MFG for the duration of the script.
		GetInventory strMfg,"//cods/cod[@mfg='" & strMfg &"']/init/cmds",True

		'//If the isMFG var is true (set by GetInventory) then 
		'//we know the manf/model of the COD that answered the INIT commands.
		'//Have the function return the name of the Manufacturer 
		if isMFG=True then
			GetMFG = strMFG
			ScreenEcho "COD Manufacturer Determined: " & strMFG
			'//We also want to inventory the current COM port that was used 
			'//to determine the COM port.  Add the current COM port to the 
			'//colInventory collection
			screenEcho "Adding " & oComPort.DeviceID & " to inventory"
			colInventory.Add "dCOM",oComPort.DeviceID
			Exit Function
		else
			'//If isMFG does not comea
			'//Destroy the current MFG collection
			'//so it can be rebuilt for the next mfg
			set colcmdConsts = Nothing
		end if
	
	Next

Next


if isNull(strMFG) then
	strMSG = "Could not determine COD MFG...Terminating script"
	wscript.echo strMSG
	sh.LogEvent 1,strMsg
	
	Wscript.quit(1)
	
end if

End Function


'////////////////////////////////////////////
'// Name: 	GetInventory
'// Func:   	Get the COD Inventory info and put it into a collection
'// Args:	strMfg 	- Name of COD Manufacturer
'//		xmlPath - XPATH to COD MFG Initialization/Inventory strings
'// 		bInit 	- {TRUE|FALSE} 
'//			  TRUE - MFG Initialization TEST
'//			  FALSE - MFG Inventory
'///////////////////////////////////////////

Sub GetInventory(strMfg,xmlPath,bInit)

on Error Resume Next

'//If this is not an INIT process then 
'//Add the Current MFG to the MIF File Collection
if Not bInit then colInventory.Add "dMFG",strMfg

' Read commands set from XML, then build and send command to COD and write to MIF
set oCods = xmlDoc.selectNodes(xmlPath)

'//Loop through each the COD Inventory command sets
For each oCod in oCods	
	strCodDesc = oCod.GetAttribute("desc")
	intSleep = Cint(oCod.GetAttribute("sleep"))
	
	set oCmdStrings = oCod.ChildNodes
	
	'//Enumerate the cmdstrings for the current COD for each specific command string
	For each oCmdString in oCmdStrings
		strType = oCmdString.GetAttribute("type")
		strCmd = oCmdString.text
		
		'//Create the command string by concatenating all of the commands
		'//in the child nodes.
		Select Case strType
			Case "int"
				strValue = chr(cint(colcmdConsts.Item(strCmd)))
			Case "hex"
				strValue = chr(hex(colcmdConsts.Item(strCmd)))
			Case "string"
				strvalue = colcmdConsts.Item(strCmd)
		End Select
				
		strOutput = strOutput & strValue
		
	Next
	
	'//Clear the serial port input buffer
	com.InBufferCount = 0
	
	'//echo the output string to the console
	ScreenEcho "POS-->COD: " & vbtab & strOutput
	
	'//Send it to the com port
	com.Output = strOutput
	
	'//Wait the defined number of seconds before we start to look for a return
	Wscript.Sleep (intSleep)
	
	'//Get the input back from the COD device
	strBuffer = strBuffer & com.Input

	'//parse the input based upon the mfg...each as their own custom format
	'//for returning the data
	If Len(strBuffer) > 1 Then
		Select Case strMFG
			Case "Everbrite"
				intStart = 6 '<--Need to Fix 
				intStop = Len(strBuffer) - intStart - 1
				strBuffer = Mid(strBuffer,intStart,intStop)
			Case "TexasDigital"
				intStop = instr(1,strBuffer,chr(13))
				strBuffer = Mid(strBuffer,1,intStop-1)
			Case "Delphi"

				'//Delphi delivers all its data in a single string
				'//need to break out the string and write to the 
				'//colInventory
				
				if not bInit then
					intPOSType = instr(1,strBuffer,"-")
					intFirmware = instr(intPOSType,strBuffer,":")
					intPOSType2 = instr(intFirmware,strBuffer,"[")
					intAppDateTime = instr(intPOSType2,strBuffer,"]")
					intOCSDateTime = instr(intAppDateTime,strBuffer,"(")
					intOCSResolution = instr(intOCSDateTime,strBuffer,")")
					intScreenSize = instr(intOCSResolution,strBuffer," ")
					
					strPOSType = Mid(strBuffer,2,intPOSType - 2)
					strFirmware = Mid(strBuffer,intPOSType + 3,intFirmware - 1 - intPOSType - 2)
					strPOSType2 = Mid(strBuffer,intFirmware + 1,intPOSType2 - intFirmware - 1)
					strAppDateTime = Mid(strBuffer,intPOSType2 + 1 ,intAppDateTime-intPOSType2 - 1)
					strOCSDateTime = Mid(strBuffer,intAppDateTime + 2,intOCSDateTime-intAppDateTime - 2)
					strOCSResolution = Mid(strBuffer,intOCSDateTime + 1,intOCSResolution - intOCSDateTime -1)
					strScreenSize = Mid(strBuffer,intScreenSize + 1,intScreenSize-intOCSResolution + 1)

							
					colInventory.Add "dPOSTYPE",strPOSType
					colInventory.Add "dFW",strFirmware
					colInventory.Add "dSS",strScreenSize
					colInventory.Add "dRES",strOCSResolution
					colInventory.Add "dODT",strOCSDateTime
					colInventory.Add "dADT",strAppDateTime

					ScreenEcho "POS<--COD: " & colDescs.Item(strCodDesc) & ":" & vbtab & strBuffer
				
					Exit Sub
				End if
				
		End Select
	end if
	ScreenEcho "POS<--COD: " & colDescs.Item(strCodDesc) & ":" & vbtab & strBuffer
	
	'//Add the returned innventory info to the colInventory collection to be processed
	'//turned into a mif file
	if not bInit then 
		colInventory.Add strCodDesc,strBuffer 
	else
		'//If we are running an init/discovery process...
		'//if we get something back then we are going to set the current MFG to the MFG
		'//Next time this sub is called it will enumerate the inventory commands vs. the inits
		if Len(strBuffer) > 1 then isMFG = True	
	End if
	
	'//Clear the output and buffer vars.
	strOutput = null 
	strBuffer = null
Next
End sub

'///////////////////////////////////////////////////////////
'// Name:	UpdateWMI
'// Func:	Writes the COD data to the MCD_CustomerOrderData 
'//			WMI class
'///////////////////////////////////////////////////////////

Sub UpdateWMI

Dim strClass
Dim objWMIService
Dim oProp
Dim oDesc
Dim strDescKey
	
strClass = xmlDoc.SelectSingleNode("//wmiclass").text

'//Check and see if the MCD_COD WMI class exists
'//If it doesn't exist create it
CreateCODClass

ScreenEcho "Writing COD Inventory to WMI Class " & strclass

'//SpawnInstance of the MCD COD Class
'//This overwrites any existing WMI instance
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\.\root\cimv2:" & strClass).SpawnInstance_

'//Enumerate the properties in the wmi MCD_COD Class
For each oProp in objWMIService.Properties_
	
	'//Set the key value TYPE ...The rest of the values are defined in
	'//the descriptions nodes of the COD.XML file
	if oProp.Name = "Type" then
		ScreenEcho "Writing type value"
		oProp.value = 1
	else
		'//Find the COD Description that matches the Current WMI attribute name
		'//Enumerate the COD attribute descriptions defined in the cod.xml for COD's
		'//If the Current COD description matches the current WMI attribute
		'//then record the key for the current description so we can look up the
		'//actual value in the inventory collection
		for each oDesc in colDescs
			if colDescs.Item(oDesc) = oProp.Name then
				strDescKey = oDesc
				Exit For			
			end if
			strDescKey = "$FALSE$"
		Next
		
		if strDescKey = "$FALSE$" then
			ScreenEcho "ERROR - Could not resolve WMI property " _
				& oProp.Name & " to a COD Description"
		else
			ScreenEcho "Attribute-Value: " & oProp.Name & "-" & colInventory.Item(strDescKey)
			'//Write the Inventory value for the current description to the 
			'//current wmi property	
			oProp.value = colInventory.Item(strDescKey)
		End if
	End if
Next

'//Commit all updates to the WMI Class Instance
objWMIService.Put_

End Sub



'////////////////////////////////////////////
'// Name:	CreateMIF
'// Func:	Creates the MIF file based upon 
'//			info returned and places it in directory
'//			described in COD.XML file
'/////////////////////////////////////////
Sub CreateMIF

'//Create a collection for the MIF file
'//Start to Create the MIF File
'//Add the Component Heading
'//Create the MIF file and write the header information

Dim oMIF
Dim strMIFPath
Dim strMIFFile
Dim strMIFFileSpec
Dim mifAttrib

'//Read the MIF info from the COD.XML
strMIFPath = xmlDoc.selectSingleNode("//mif").getAttribute("path")
strMIFName = xmlDoc.selectSingleNode("//mif").text
strMIFFileSpec = strMIFPath & "\" & strMIFName

'//Attempt to Create the MIF file.
if fs.FolderExists(strMIFPath) then
	set oMIF = fs.CreateTextFile(strMIFFileSpec,true)
else
	strMsg = strMIFPath & " does not exist.  The MIF file cannot be created."
	sh.LogEvent 1,strMsg
	ScreenEcho strMsg
	Exit sub
end if


'//Write out the MIF data to the newly created file
oMif.WriteLine "Start Component"
oMif.WriteLine vbTab & "Name = " & chr(34) & "Custom Inventory" & chr(34)

'//Set the counters
intIDCounter = 0
intAttribCounter = 1
'//Start a new group and add the group info
		'//	Class
		'//	ID
		'//	Key
		oMif.WriteLine vbTab & "Start Group"

		'//Write the Group to the MIF
		oMif.WriteLine vbtab & vbTab & "Name = " & chr(34) & "CUSTOMER ORDER DISPLAY" & chr(34)
		
		'//Write the ID use the Heading counter
		oMif.WriteLine vbtab & vbTab & "ID = " & intIDCounter + 1

		'//Write the class name
		oMif.WriteLine vbtab & vbTab & "Class = " & chr(34) & "Customer Order Display" & chr(34)
		
		'//Write the key - use the heading counter
		'//Setting the KEY to one - we really don't have this
		'//info in the ini so just default it to 1
		oMif.writeline vbtab & vbTab & "Key = 1" 

'//Enumerate the Inventory collection we created and write it to the body of the MIF
For each mifAttrib in colInventory

	'//Write the Data From the COD to the MIF
	'//Write all of the attribute information
	'//Write start attribute
	oMif.writeline vbtab & vbTab & "Start Attribute"
			
	'//Write the name value to the MIF
	oMif.WriteLine vbTab & vbtab & vbTab & "Name = " & chr(34) & colDescs.item(mifAttrib) & chr(34)	

	'//Write the attribute ID - use the attrib counter
	oMif.writeline vbTab & vbtab & vbTab & "ID = " & intAttribCounter
	
	'//Write the variable type - String for everyone
	Dim intLength 
	intLength = Len(colInventory.Item(mifAttrib))
	oMif.writeline vbTab & vbtab & vbTab & "Type = String(" & intLength & ")"		
	
	'//Write the name value value to the MIF
	oMif.WriteLine vbTab & vbtab & vbTab & "Value = " & chr(34) & colInventory.Item(mifAttrib) & chr(34)	
			
	'//Close the attribute
	oMif.WriteLine  vbTab & vbtab & "End Attribute"

	intAttribCounter = 1 + intAttribCounter
	
	strBuffer = Null

Next

'//Close the Group
oMif.WriteLine vbTab & "End Group"
'//Close the MIF
oMif.WriteLine "End Component"
'//Close the file
oMif.close

End Sub


'//////////////////////////////////////
'// Name:	CreateDescCol
'// Func:	Reads COD Descriptions from the COD.XML and
'//		puts them into the colDescs collection/dictionary object
'//		These descriptions become the MIF/WMI attribute names
'//////////////////////////////////////

Sub CreateDescCol

	'//Create a collection of description constants all the COD constants from the XML
	set oConstNodes = xmlDoc.selectNodes("//cmdconsts/category[@id='Descriptions']/cmdconst") 
	set colDescs = CreateObject("Scripting.Dictionary") 
	For each oConstNode in oConstNodes
		colDescs.Add oConstNode.GetAttribute("id"),oConstNode.GetAttribute("value")
	Next 

End sub


'//////////////////////////////////////
'// Name:	CreateMfgCol
'// Func:	Reads MFG specfic COD constants from the COD.XML and
'//		puts them into the colCmdConsts collection/dictionary object
'//		These are the COD serial commands used by GetInventory
'// Args:	strMFG - Name of the manufacturer - must match the MFG attribute 
'//		in XML
'//////////////////////////////////////
Sub CreateMfgCol(strMfg)

	'//Build dictionary of MFG constants
	set oConstNodes = xmlDoc.selectNodes("//cmdconsts/category[@id='" & strMfg & "']/cmdconst") 
	set colcmdConsts = CreateObject("Scripting.Dictionary") 
	For each oConstNode in oConstNodes
		colcmdConsts.Add oConstNode.GetAttribute("id"),oConstNode.GetAttribute("value")
	Next 

End sub



'/////////////////////////////////////////
'// Name:	CreateCODClass
'// Func:	Creates the MCD_CustomerOrderDisplay WMI Class
'//		from current descriptions in the COD.XML file
'//////////////////////////////////////////

Sub CreateCODClass

	Dim loc
	Dim wbemServices
	Dim wbemObject
	Dim oDesc
	Dim strClass
	
	strClass = xmlDoc.SelectSingleNode("//wmiclass").text
	
	Set loc = CreateObject("WbemScripting.SWbemLocator")
	Set WbemServices = loc.ConnectServer(, "root\CIMv2")
	On Error Resume Next
	Set WbemObject = WbemServices.Get(strClass)
	
	If Err Then
		Err.Clear
		strMsg = "MCD_Customer_Order_Display does not exist...Attempting to create:"
		'//Retrieve blank class
		Set WbemObject = WbemServices.Get
		'//Set class name
		WbemObject.Path_.Class = strClass
		'//Add Properties (8 = CIM_STRING, 11 = CIM_BOOLEAN)
		WbemObject.Properties_.Add "Type", 19
		
		'//Enumerate the descriptions in the COD.xml 
		'//create a string attribute for each one.
		For each oDesc in colDescs.Items
			WbemObject.Properties_.Add oDesc, 8
			strMsg = strMsg & vbcrlf & vbtab & "Adding Attribute: " & oDesc
		Next
		
		'//Set the key
		WbemObject.Properties_("Type").Qualifiers_.Add "key", True

		'//Commit the data
		WbemObject.Put_
		HandleErr 
			
	End if

End Sub


'//////////////////////////////////////////
'// Name:	HandleErr
'// Func: 	Simple Err Handler
'//////////////////////////////////////////

Sub HandleErr()

Dim bReturn
if Err.Number = 0 then 
	ScreenEcho "Success: " & strMsg
	sh.logevent 0, "Success: " & strMsg
else
	ScreenEcho "Error: " & strMsg
	wscript.echo vbtab & "Error Number: " & err.number
	wscript.echo vbtab & "Error Description: " & err.Description
	
	sh.logEvent 1, "Error: " & strMsg & vbcrlf & "Error Number: " _
			& err.number & vbcrlf & "Error Description: " & err.Description
	
	if bPauseOnErr then
		bReturn = MsgBox ("The following error has occuring while setting IP Address Info: " _
			& vbcrlf & vbcrlf & "Error: " & strMsg _
			& vbcrlf & "Error Number: " & err.Number _
			& vbcrlf & "Error Description: " & err.Description _
			& vbcrlf & vbcrlf & "Would you like to continue the configuration of IP Settings?",vbYesNo,"ConfigVS Error")

		if bReturn <> 6 then wscript.quit(1)
	End if
end if
Err.Clear
End sub

'//////////////////////////////////////////
'// Name:	ScreenEcho
'// Func: 	Echo test to console with date/time
'//////////////////////////////////////////

Sub ScreenEcho (strText)
	wscript.echo Now & ": " & strText
End Sub
