# Copyright (c) 1993-1999 Microsoft Corp.
#
# This is a sample LMHOSTS file used by the Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to computernames
# (NetBIOS) names.  Each entry should be kept on an individual line.
# The IP address should be placed in the first column followed by the
# corresponding computername. The address and the computername
# should be separated by at least one space or TAP. The "#" character
# is generally used to denote the start of a comment (see the exceptions
# below).
#
# This file is compatible with Microsoft LAN Manager 2.x TCP/IP lmhosts
# files and offers the following extensions:
#
#      #PRE
#      #DOM:<domain>
#      #INCLUDE <filename>
#      #BEGIN_ALTERNATE
#      #END_ALTERNATE
#      \0xnn (non-printing character support)
#
# Following any entry in the file with the characters "#PRE" will cause
# the entry to be preloaded into the name cache. By default, entries are
# not preloaded, but are parsed only after dynamic name resolution fails.
#
# Following an entry with the "#DOM:<domain>" tag will associate the
# entry with the domain specified by <domain>. This affects how the
# browser and logon services behave in TCP/IP environments. To preload
# the host name associated with #DOM entry, it is necessary to also add a
# #PRE to the line. The <domain> is always preloaded although it will not
# be shown when the name cache is viewed.
#
# Specifying "#INCLUDE <filename>" will force the RFC NetBIOS (NBT)
# software to seek the specified <filename> and parse it as if it were
# local. <filename> is generally a UNC-based name, allowing a
# centralized lmhosts file to be maintained on a server.
# It is ALWAYS necessary to provide a mapping for the IP address of the
# server prior to the #INCLUDE. This mapping must use the #PRE directive.
# In addtion the share "public" in the example below must be in the
# LanManServer list of "NullSessionShares" in order for client machines to
# be able to read the lmhosts file successfully. This key is under
# \machine\system\currentcontrolset\services\lanmanserver\parameters\nullsessionshares
# in the registry. Simply add "public" to the list found there.
#
# The #BEGIN_ and #END_ALTERNATE keywords allow multiple #INCLUDE
# statements to be grouped together. Any single successful include
# will cause the group to succeed.
#
# Finally, non-printing characters can be embedded in mappings by
# first surrounding the NetBIOS name in quotations, then using the
# \0xnn notation to specify a hex value for a non-printing character.
#
# The following example illustrates all of these extensions:
#
# 102.54.94.97     rhino         #PRE #DOM:networking  #net group's DC
# 102.54.94.102    "appname  \0x14"                    #special app server
# 102.54.94.123    popular            #PRE             #source server
# 102.54.94.117    localsrv           #PRE             #needed for the include
#
# #BEGIN_ALTERNATE
# #INCLUDE \\localsrv\public\lmhosts
# #INCLUDE \\rhino\public\lmhosts
# #END_ALTERNATE
#
# In the above example, the "appname" server contains a special
# character in its name, the "popular" and "localsrv" server names are
# preloaded, and the "rhino" server name is specified so it can be used
# to later #INCLUDE a centrally maintained lmhosts file if the "localsrv"
# system is unavailable.
#
# Note that the whole file is parsed including comments on each lookup,
# so keeping the number of comments to a minimum will improve performance.
# Therefore it is not advisable to simply add lmhosts file entries onto the
# end of this file.

10.45.254.71	EU04137GSC01
10.45.254.201	EU04137POS01
10.45.254.202	EU04137POS02
10.45.254.203	EU04137POS03
10.45.254.204	EU04137POS04
10.45.254.205	EU04137POS05
10.45.254.206	EU04137POS06
10.45.254.207	EU04137POS07
10.45.254.208	EU04137POS08
10.45.254.209	EU04137POS09
10.45.254.210	EU04137POS10
10.45.254.211	EU04137POS11
10.45.254.212	EU04137POS12
10.45.254.213	EU04137POS13
10.45.254.214	EU04137POS14
10.45.254.215	EU04137POS15
10.45.254.216	EU04137POS16
10.45.254.217	EU04137POS17
10.45.254.218	EU04137POS18
10.45.254.219	EU04137POS19
10.45.254.220	EU04137POS20
10.45.254.145	EU04137TAP45
10.45.254.146	EU04137TAP46
10.45.254.147	EU04137TAP47
10.45.254.148	EU04137TAP48
10.45.254.149	EU04137TAP49
10.45.254.101	EU04137KVS01
10.45.254.102	EU04137KVS02
10.45.254.103	EU04137KVS03
10.45.254.104	EU04137KVS04
10.45.254.105	EU04137KVS05
10.45.254.106	EU04137KVS06
10.45.254.107	EU04137KVS07
10.45.254.108	EU04137KVS08
10.45.254.109	EU04137KVS09
10.45.254.110	EU04137KVS10
10.45.254.111	EU04137KVS11
10.45.254.112	EU04137KVS12
10.45.254.113	EU04137KVS13
10.45.254.114	EU04137KVS14
10.45.254.115	EU04137KVS15
10.45.254.116	EU04137KVS16
10.45.254.117	EU04137EPD01
10.45.254.118	EU04137EPD02
10.45.254.119	EU04137EPD03
10.45.254.120	EU04137EPD04
10.45.254.121	EU04137ORB01
10.45.254.122	EU04137ORB02
10.45.254.193	EU04137CSO21
10.45.254.194	EU04137CSO22
10.45.254.195	EU04137CSO23
10.45.254.196	EU04137CSO24
10.45.254.197	EU04137CSO25
10.45.254.198	EU04137CSO26